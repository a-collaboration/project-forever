#version 150

varying vec4 colour;
varying vec2 texCoord;

uniform sampler2D tex;
uniform float u_alpha;

void main(void)
{
    gl_FragColor = texture2D(tex, texCoord) ;
    gl_FragColor.a *= u_alpha;
}
