#version 150

varying vec4 colour;
varying vec2 texCoord;

uniform sampler2D tex;
uniform vec4 u_colour;

void main(void)
{
    vec4 c = u_colour * colour;
    gl_FragColor = texture2D(tex, texCoord) * c;
}
