/*!
 * Structs for the main project. Will smuggle in the required headers for the moment.
 * As a rule, structs relating to the game are pseudo-namespaced with `PF_`.
 *
 * Everything is also forward-declared at the top for readability and encapsulation.
 * Variables are snake_case, structs PascalCase, with pointer variables prefixed with `p_`.
 */

#pragma once

// C++ header guards.
#ifdef __cplusplus
extern "C" {
#endif

// Look into making these includes lighter-weight.
#include "defs.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_gpu.h>
#include <box2d/box2d.h>


/* Forward declarations */
/* Overall/Graphics */
typedef struct PF_Game PF_Game;
typedef struct PF_DelStack PF_DelStack;
typedef struct PF_AtlasImage PF_AtlasImage;
typedef struct PF_AtlasEntry PF_AtlasEntry;
typedef struct PF_ShaderData PF_ShaderData;
/* Gameplay */
typedef struct PF_World PF_World;
typedef struct PF_Entity PF_Entity;
typedef struct PF_Tile PF_Tile; // This is defined in tiles.h.
typedef struct PF_SaveData PF_SaveData; // This is defined in data.h.
typedef struct mu_Context mu_Context; // A context for microui.

/* Structs */
/* Relating to the overal game/application. */
struct PF_Game{
    /* Rendering */
    SDL_Window *p_window;
    GPU_Target *p_screen;
    /* Delegate for modal functions */
    struct Delegate {
        void (*logic)(void);
        void (*draw)(void);
    } delegate;
    /* Sprite atlas */
    PF_AtlasImage *p_sprites;
    PF_AtlasEntry *p_sprite_atlas;
    GPU_Image *p_atlas_texture;
    /* Tile Data */
    size_t n_tiles;
    PF_Tile *p_tile_array;
    /* Shader Blocks */
    size_t n_shaders;
    PF_ShaderData *p_shaders;
    /* Input */
    struct Input { // This might become an array if we ever need multiplayer.
        bool keyboard[300];
        bool mouse_pressed[6];
        int mouse_x;
        int mouse_y;
        float mouse_x_meter;
        float mouse_y_meter;
        float mouse_wheel;
    } input;
    /* Scaled coords */
    struct Scaled_Coords {
        float x, y, w, h, w_meter, h_meter;
    } scaled;
    /* microui / GUI */
    _Bool using_ui;
    mu_Context *p_ctx;
    /* Cleanup */
    PF_DelStack *p_deletion_stack;
};

struct PF_DelStack{
    void (*fun)(void);
    PF_DelStack *p_next;
};

struct PF_AtlasImage{
    char filename[MAX_FILENAME_LENGTH];
    GPU_Rect rect;
    GPU_Image *texture;
};

struct PF_AtlasEntry{
    char* key;
    PF_AtlasImage value;
};

struct PF_ShaderData {
    uint32_t shader;
    GPU_ShaderBlock block;
    int n_res;
    int *p_res;
};

/* Relating to gameplay/levels/stages. */
struct PF_Entity {
    float x;
    float y;
    int w;
    int h;
    PF_AtlasImage *texture;
    b2BodyId body_id;
    b2ShapeId shape_id;
    struct PF_Entity *next;
};

struct PF_World {
    // For graphics and the entity system. For simplicity, entities are a linked list for the time being.
    PF_Entity entity_head;
    PF_Entity *p_entity_tail;
    // For the physics engine.
    b2WorldId world_id;
    size_t n_terrain;
    b2BodyId *p_terrain_id;
    b2ShapeId *p_terrain_shape_id;
    uint32_t *p_terrain_data;
};

#ifdef __cplusplus
}
#endif
