/*!
 * Tile-based map editor.
 * Tiled wasn't invented here.
 * A simple tool for putting together map tiles and getting maps into our desired binary format.
 */

#include <SDL2/SDL_gpu.h>

#include <SDL_scancode.h>
#include <stdint.h>
#include <stdio.h>
#include <stb/stb_ds.h>
#include <microui/microui.h>

#include "mapedit.h"
#include "../structs.h"
#include "../globals.h"
#include "../engine/init.h"
#include "../engine/data.h"
#include "../engine/tiles.h"
#include "../engine/draw.h"
#include "../engine/ui.h"

// For optimisation, get all of this memory contiguous and just use pointer arithmetic.
// The tile array crams all tile-specific data into one int field.
// The leftmost-two bits are the facing; the remainder 
PF_SaveData data;
const char *file_path = NULL;

// Define a camera for scrolling.
struct Point {
    int x, y;
} camera = {};

// Track the last edited tile to avoid immediate repeats.
uint32_t current_tile_add = -1;
uint32_t current_tile_remove = -1;

uint32_t selected_tile_id = 1;
enum tile_facing tile_facing = TILE_FACING_UP;

static void edit_logic(void);
static void edit_draw(void);
static void cleanup_editor(void);

void init_editor(const char *path)
{
    // Set the file path.
    file_path = (path == NULL) ? "stg/testmap.pfm" :  path;
    printf("%s\n", file_path);
    // Attempt an initial load only if a path is specified.
    if (path != NULL) {
        data = load_map(path);
    }
    // Do a default initialisation if there is no save.
    if (!data.tile_arr) {
        data.tile_width = N_TILES_WIDTH;
        data.tile_height = N_TILES_HEIGHT;
        data.tile_arr = calloc(sizeof(uint32_t), data.tile_width * data.tile_height);
    }
    camera.y = -12;
    init_ui();
    push_deletion(cleanup_editor);
}

void mode_editor_activate(void)
{
    game.delegate.logic = edit_logic;
    game.delegate.draw = edit_draw;
}

/* Logic/UI Functions */
static void ui_tile_picker(void);
static void ui_save_load(void);

static void edit_logic(void)
{
    // First handle the UI.
    // Avoid a VLA and just keep these lengths synced normally.
    void (*ui_funcs[2])(void) = {ui_tile_picker, ui_save_load};
    process_ui(2, ui_funcs);
    // Preserve the test behaviour of exit-on-space.
    if (game.input.keyboard[SDL_SCANCODE_SPACE])
    {
        exit(0);    
    }
    // Basic behaviour: which tile is selected.
    int tile_x, tile_y;
    tile_x = (game.input.mouse_x - camera.x) / TILE_SIZE;
    tile_y = (game.input.mouse_y - camera.y) / TILE_SIZE;
    if (tile_x >= 0 && tile_y >= 0 && tile_x < data.tile_width && tile_y < data.tile_height) // Check bounds.
    {
        uint32_t curr = tile_x + data.tile_width * tile_y;
        if (game.input.mouse_pressed[SDL_BUTTON_LEFT])
        {
            if (current_tile_add != curr) {
                uint32_t facing = tile_facing; // Use the selected tile facing.
                data.tile_arr[tile_x + data.tile_width * tile_y] = (facing << 30) | selected_tile_id;
                current_tile_add = curr;
            }
        }
        if (game.input.mouse_pressed[SDL_BUTTON_RIGHT])
        {
            if (current_tile_remove != curr) {
                data.tile_arr[tile_x + data.tile_width * tile_y] = 0; // Remove the tile.
                current_tile_remove = curr;
            }
        }
        // Select tile with mouse wheel.
        if (game.input.mouse_wheel)
        {
            int signum = game.input.mouse_wheel > 0 ? 1 : -1;
            selected_tile_id = (game.n_tiles + selected_tile_id + signum) % game.n_tiles;
            game.input.mouse_wheel = 0;
        }
        // Set facing with F1-F4.
        if (game.input.keyboard[SDL_SCANCODE_F1]) {
            game.input.keyboard[SDL_SCANCODE_F1] = 0;
            tile_facing = TILE_FACING_UP;
        }
        if (game.input.keyboard[SDL_SCANCODE_F2]) {
            game.input.keyboard[SDL_SCANCODE_F2] = 0;
            tile_facing = TILE_FACING_LEFT;
        }
        if (game.input.keyboard[SDL_SCANCODE_F3]) {
            game.input.keyboard[SDL_SCANCODE_F3] = 0;
            tile_facing = TILE_FACING_DOWN;
        }
        if (game.input.keyboard[SDL_SCANCODE_F4]) {
            game.input.keyboard[SDL_SCANCODE_F4] = 0;
            tile_facing = TILE_FACING_RIGHT;
        }
    }
    // Have some test camera behaviour:
    camera.x += game.input.keyboard[SDL_SCANCODE_LEFT]*12 + game.input.keyboard[SDL_SCANCODE_RIGHT]*-12;
    camera.y += game.input.keyboard[SDL_SCANCODE_UP]*12 + game.input.keyboard[SDL_SCANCODE_DOWN]*-12;
    // Save on w.
    if (game.input.keyboard[SDL_SCANCODE_W]) {
        game.input.keyboard[SDL_SCANCODE_W] = 0;
        save_map(file_path, "m",  data.tile_width, data.tile_height, data.tile_arr);
    }
}

static void ui_tile_picker(void)
{
    if (mu_begin_window(game.p_ctx, "Tile Picker", mu_rect(20, 20, 200, 220)))
    {
        mu_layout_row(game.p_ctx, 1, (int[]){ -1 }, 14);
        mu_label(game.p_ctx, "Tile");
        mu_layout_row(game.p_ctx, 1, (int[]){ -1 }, -50);
        mu_begin_panel(game.p_ctx, "tile_select");
        mu_Container *panel = mu_get_current_container(game.p_ctx);
        int tile_id = -1;
        for (int i = 0; i < game.n_tiles/3; ++i)
        {
            mu_layout_row(game.p_ctx, 3, (int[]){ -134, -67, -1 }, 0);
            for (int j = 1; j <= 3; ++j) {
                if (mu_picture_toggle(game.p_ctx, game.p_tile_array[3*i+j].texture->filename, selected_tile_id == 3*i+j))
                {
                    tile_id = 3*i+j;
                }
            }
        }
        switch (tile_id)
        {
            case -1:
                break;
            default:
                selected_tile_id = tile_id;
                break;
        }
        mu_end_panel(game.p_ctx);
        mu_layout_row(game.p_ctx, 1, (int[]){ -1 }, 0);
        mu_label(game.p_ctx, "Facing");
        mu_layout_row(game.p_ctx, 4, (int[]){-150, -100, -50, -1}, 0);
        int facing = -1;
        if (mu_button_ex(game.p_ctx, "Up", tile_facing == TILE_FACING_UP ? MU_ICON_CLOSE : 0, 0)) { facing = TILE_FACING_UP; }
        if (mu_button_ex(game.p_ctx, "Left", tile_facing == TILE_FACING_LEFT ? MU_ICON_CLOSE : 0, 0)) { facing = TILE_FACING_LEFT; }
        if (mu_button_ex(game.p_ctx, "Down", tile_facing == TILE_FACING_DOWN ? MU_ICON_CLOSE : 0, 0)) { facing = TILE_FACING_DOWN; }
        if (mu_button_ex(game.p_ctx, "Right", tile_facing == TILE_FACING_RIGHT ? MU_ICON_CLOSE : 0, 0)) { facing = TILE_FACING_RIGHT; }
        switch (facing)
        {
            case -1:
                break;
            default:
                tile_facing = facing;
                break;
        }
        mu_end_window(game.p_ctx);
    }
}

static void ui_save_load(void)
{
    // Very basic. Just a matter of inputting a path.
    // Currently does minimal validation.
    if (mu_begin_window(game.p_ctx, "Save/Load Room", mu_rect(20, 250, 200, 70)))
    {
        mu_layout_row(game.p_ctx, 1, (int[]){-1}, -18);
        static char buf[MAX_FILENAME_LENGTH];
        int save_load = 0;
        if (mu_textbox(game.p_ctx, buf, sizeof(buf)) & MU_RES_SUBMIT) 
        {
            mu_set_focus(game.p_ctx, game.p_ctx->last_id);
            save_load = 1;
        }
        mu_layout_row(game.p_ctx, 2, (int[]){-100, -1}, 0);
        if (mu_button(game.p_ctx, "Save")) { save_load = 1; }
        if (mu_button(game.p_ctx, "Load")) { save_load = 2; }
        switch (save_load)
        {
            default:
                break;
            case 1:
                save_map(buf, "m",  data.tile_width, data.tile_height, data.tile_arr);
                break;
            case 2:
                free_save_data(&data);
                data = load_map(buf);
                break;
        }
        mu_end_window(game.p_ctx);
    }
}

/* Draw Functions */

static void draw_usable_area(int, int, int);
static void draw_tiles(int, int, int);
static void draw_grid(int, int, int);
static void draw_ghost(int, int, int);

static void edit_draw(void)
{
    draw_usable_area(TILE_SIZE, data.tile_width, data.tile_height);
    draw_tiles(TILE_SIZE, data.tile_width, data.tile_height);
    draw_grid(
        TILE_SIZE,
        game.scaled.w/TILE_SIZE+1, 
        game.scaled.h/TILE_SIZE+1
    );
    draw_ghost(TILE_SIZE, data.tile_width, data.tile_height);
    draw_ui();
}

static void draw_usable_area(int dimension, int tiles_w, int tiles_h)
{
    // Highlight the playable area.
    GPU_ClearRGB(game.p_screen, 0xff, 0, 0);
    GPU_RectangleFilled(
        game.p_screen, 
        camera.x,
        camera.y, // Encode the offset for variable-sized rooms.
        dimension*tiles_w + camera.x,
        dimension*tiles_h + camera.y,
        (SDL_Colour){25,25,25,0xff} // Vim throws a fit about this for some reason.
    );

}

static void draw_tiles(int dimension, int tiles_w, int tiles_h)
{
    for (int i = 0; i < tiles_w; ++i) {
        for (int j = 0; j < tiles_h; ++j)
        {
            uint32_t curr = data.tile_arr[i+tiles_w*j];
            if ((curr & ~TILE_FACING_MASK) != 0)
            {
                blit_image(
                    game.p_tile_array[curr & ~TILE_FACING_MASK].texture,
                    camera.x + dimension * i + dimension * 0.5,
                    camera.y + dimension * j + dimension * 0.5,
                    ((curr & TILE_FACING_MASK) >> 30) * PI/2.f, 
                    1
                );
            }
        }
    }
}

static void draw_grid(int dimension, int tiles_w, int tiles_h)
{
    // Draw a tile grid. Future-proofed to allow for subdivisions.
    SDL_Colour grey = {0xa0, 0xa0, 0xa0, 0xff};
    for (int i = 0; i < tiles_w; ++i)
    {
        GPU_Line(game.p_screen, 
                (camera.x % dimension) + i * dimension,
                0,
                (camera.x % dimension) + i * dimension,
                game.scaled.h,
                grey
        );
    }
    for (int i = 0; i < tiles_h; ++i)
    {
        GPU_Line(game.p_screen,
            0,
            (camera.y % dimension) + i*dimension,
            game.scaled.w,
            (camera.y % dimension) + i*dimension,
            grey
        );
    }
}

static void draw_ghost(int dimension, int tiles_w, int tiles_h)
{
    if (!selected_tile_id) {
        return;
    }
    // Draw a preview of the next tile to be placed.
    int tile_x, tile_y;
    tile_x = (game.input.mouse_x - camera.x) / dimension;
    tile_y = (game.input.mouse_y - camera.y) / dimension;
    if (tile_x >= 0 && tile_y >= 0 && tile_x < tiles_w && tile_y < tiles_h) // Check bounds.
    {
        activate_shader(SHADER_OPACITY);
        GPU_SetUniformf(game.p_shaders[SHADER_OPACITY].p_res[0], 0.5);
        blit_image(
                game.p_tile_array[selected_tile_id].texture,
                camera.x + (tile_x + 0.5) * dimension,
                camera.y + (tile_y + 0.5) * dimension,
                tile_facing * PI/2,
                1);
        GPU_ActivateShaderProgram(0, NULL);
    }
}

static void cleanup_editor(void)
{
    free_save_data(&data);
}
