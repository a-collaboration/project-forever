/*!
 * Interface for map editor mode.
 * Designed to be loaded as a PF_Game and to run in that same environment.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif



/*!
 * Initialise the state for the editor and queue its ending.
 * @param path The path to the map to edit and to which to save.
 */
void init_editor(const char *);

/*!
 * Enable editor mode.
 * Sets the appropriate draw and logic functions to allow for map editing.
 */
void mode_editor_activate(void);

#ifdef __cplusplus
}
#endif
