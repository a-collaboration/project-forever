#include "level.h"
#include "../structs.h"
#include "../engine/init.h"
#include "../engine/draw.h"
#include "../engine/input.h"
#include "../engine/text.h"
#include <box2d/manifold.h>
#include <box2d/types.h>
#include <stb/stb_ds.h>

#include "../globals.h"

#include <box2d/box2d.h>
#include <box2d/geometry.h>

#include "../engine/entity.h"
#include "../engine/camera.h"
#include "../engine/tiles.h"
#include "../engine/data.h"
#include <SDL2/SDL.h>
#include <stdlib.h>

/*
    A demo level in which you are a fat-fridge.

    Roll the fridge with `A` and `D`.
    Shift the camera with the arrows.
    Spawn random box-props with L-click.
    Spawn wheel with R-click.
    Space to exit.
*/

// Forward static declarations.
static void game_logic(void);
static void game_draw(void);
static void init_physics(void);
static void cleanup_physics(void);
static void load_terrain(const char *);
static void load_entities(void);

// Some physics stuff
static void apply_inverse_attactor(PF_Entity *, float, float, float, float);

// Additional Functions
static const char *possible_spawn_paths[] = {
    "gfx/toaster.png",
    "gfx/fridge.png",
    "gfx/kettle.png",
    "gfx/boombox.png",
    "gfx/crate_side.png",
    "gfx/crate_front.png",
    "gfx/blender.png",
    NULL
    // Add more strings as needed
};

static const char *getRandomString(const char *strings[])
{
    int number_of_strings = 0;
    while (strings[number_of_strings] != NULL)
    {
        number_of_strings++;
    }

    // Generate a random index
    int randomIndex = rand() % number_of_strings;
    // Return the selected string
    return strings[randomIndex];
}

// Store level specific data
bool can_spawn_left_mouse;
bool can_spawn_right_mouse;

b2BodyDef body_def;
b2ShapeDef shape_def;

float zoom_factor;
float zoom_max;
float zoom_min;

PF_Entity *camera_ancor;
PF_Entity *player_entity;
b2Vec2 player_entity_position;
b2Vec2 player_movement_force;

PF_Font *test_font;

void init_game_world(void)
{
    can_spawn_left_mouse = true;
    can_spawn_right_mouse = true;

    // Setup local defs for box2D objects
    body_def = b2_defaultBodyDef;
    body_def.type = b2_dynamicBody;
    shape_def = b2_defaultShapeDef;
    shape_def.restitution = 0.2f;

    zoom_factor = 0.3;

    game.p_screen->camera.zoom_x = 1;
    game.p_screen->camera.zoom_y = 1;
    // PH
    test_font = PF_font_open(game.p_screen, "fnt/CharisSil-Jw77.ttf", 32);
    // Initialise physics. With saving/loading, this take more paramters.
    init_physics();
    push_deletion(cleanup_game_world);
}

void cleanup_game_world(void)
{
    PF_font_close(test_font);
    cleanup_physics();
}

void mode_game_activate(void)
{
    game.delegate.logic = game_logic;
    game.delegate.draw = game_draw;
}

/*
 * Startup and termination of the phsyics world.
 */
static void init_physics(void)
{
    const b2Vec2 gravity = {0.f, 10.f}; // Move this to a define or parameter.
    b2WorldDef world_def = b2_defaultWorldDef;
    world_def.gravity = gravity;
    world.world_id = b2CreateWorld(&world_def);
    // Refactor these at some stage to take in an actual chunk/zone to load.
    load_terrain("stg/demolevel.pfm");
    load_entities();
    b2Vec2 anchor_pos = {game.scaled.w_meter / 2, game.scaled.h_meter / 2};
    camera_ancor = PF_camera_follow_init(player_entity,
                                         PF_camera_follow_anchor_default(anchor_pos, true, true));
}

static void cleanup_physics(void)
{
    // Unload terrain.
    for (int i = 0; i < world.n_terrain; ++i)
    {
        b2DestroyShape(world.p_terrain_shape_id[i]);
        b2DestroyBody(world.p_terrain_id[i]); // For now assume a 1-1 correlation between bodies and shapes.
    }
    arrfree(world.p_terrain_id);
    arrfree(world.p_terrain_shape_id);
    world.n_terrain = 0;
    // Iterate over entities to unload them - free them in the meanwhile. This can be done elsewhere later.
    PF_Entity *e;
    while (world.entity_head.next)
    {
        e = world.entity_head.next;
        b2DestroyShape(e->shape_id);
        b2DestroyBody(e->body_id);
        world.entity_head.next = e->next;
        free(e);
    }
    // Finally, destroy the world.
    b2DestroyWorld(world.world_id);
    world.world_id = b2_nullWorldId;
}

// Load a room from a map, or use some static basic form if the path is NULL.
static void load_terrain(const char *path)
{

    if (path != NULL) {
        printf("Loading map: %s\n", path);
        b2BodyDef body_def = b2_defaultBodyDef;
        b2BodyId body_id;
        b2ShapeId shape_id;
        b2Vec2 tile_pos;
        b2Polygon tile_box;
        b2ShapeDef shape_def = b2_defaultShapeDef;

        // Set some constant body properties for the moment.
        body_def.type = b2_staticBody;
        shape_def.density = 1.f;
        shape_def.friction = 0.3f;

        // Load the map, iterate over the object list and spawn the tiles.
        PF_SaveData loaded = load_map(path);
        if (!loaded.tile_arr) {
            printf("Unable to load map: %s\n", path);
            return; // Maybe exit altogether.
        }
        for (int i = 0; i < loaded.tile_width; ++i)
        {
            for (int j = 0; j < loaded.tile_height; ++j)
            {
                uint32_t curr = loaded.tile_arr[i+loaded.tile_width*j];
                if ((curr & ~TILE_FACING_MASK) != 0)
                {
                    // The tile data should have been loaded at startup.
                    // Define the body.
                    body_def.position = (b2Vec2){i + 0.5, j + 0.5};
                    body_def.angle = ((curr & TILE_FACING_MASK) >> 30) * PI/2.f; // It's a square. This doesn't matter.
                    body_id = b2CreateBody(world.world_id, &body_def);
                    // Assemble the shape.
                    tile_box = get_tile_hitbox(curr);
                    shape_id = b2CreatePolygonShape(body_id, &shape_def, &tile_box);
                    // Add the objects to the world.
                    arrput(world.p_terrain_id, body_id);
                    arrput(world.p_terrain_shape_id, shape_id);
                    arrput(world.p_terrain_data, curr);
                    ++world.n_terrain;
                }
            }
        }
        free_save_data(&loaded);
    } else {
        // Use an STB array for terrain, since it's fairly static.
        // Hard-code in a flat plane.
        puts("No map provided. Loading default room.");
        b2BodyDef body_def = b2_defaultBodyDef;
        b2BodyId ground_body_id;
        b2ShapeId ground_shape_id;
        b2Vec2 ground_pos = {game.scaled.w_meter / 2.f, game.scaled.h_meter * 0.7f};

        body_def.position = ground_pos;
        body_def.type = b2_staticBody;
        ground_body_id = b2CreateBody(world.world_id, &body_def);
        arrput(world.p_terrain_id, ground_body_id);
        ++world.n_terrain;

        // b2Polygon ground_box = b2MakeBox(game.scaled.w_meter / 2.f, 15.f * METER_PER_PIXEL);
        b2Polygon ground_box = b2MakeBox(game.scaled.w_meter / 2.f, 0.5f);
        b2ShapeDef shape_def = b2_defaultShapeDef;
        shape_def.density = 1.f;
        shape_def.friction = 0.3f;
        ground_shape_id = b2CreatePolygonShape(ground_body_id, &shape_def, &ground_box);
        arrput(world.p_terrain_shape_id, ground_shape_id);
    }
}

static void load_entities(void)
{
    // Initialise the tail of the entity list pointing at the head here, for now.
    world.entity_head.next = NULL;
    world.p_entity_tail = &world.entity_head;

    // Spawn player
    body_def.position.x = game.scaled.w_meter / 2.f;
    body_def.position.y = 0;
    player_entity = spawn_box_from_image("gfx/fat_fridge.png", body_def, shape_def);
}

/*
 * Logic and draw functions. Do everything for gameplay.
 */
static void game_logic(void)
{
    // mouse wheel zoom.
    PF_camera_move_camera(0, 0, zoom_factor * game.input.mouse_wheel);

    // smoothly shift the camera based on arrow keys.
    PF_camera_follow_target_shift(10 * game.input.keyboard[SDL_SCANCODE_RIGHT] - 10 * game.input.keyboard[SDL_SCANCODE_LEFT],
                                  10 * game.input.keyboard[SDL_SCANCODE_DOWN] - 10 * game.input.keyboard[SDL_SCANCODE_UP]);

    if (game.input.keyboard[SDL_SCANCODE_SPACE]) // exit.
    {
        exit(0);
    }
    else if (game.input.mouse_pressed[SDL_BUTTON_LEFT]) // spawn random box.
    {
        if (can_spawn_left_mouse)
        {
            body_def.position.x = game.input.mouse_x_meter;
            body_def.position.y = game.input.mouse_y_meter;
            spawn_box_from_image(getRandomString(possible_spawn_paths), body_def, shape_def);
            can_spawn_left_mouse = false;
        }
    }
    else if (!game.input.mouse_pressed[SDL_BUTTON_LEFT])
    {
        can_spawn_left_mouse = true;
    }

    if (game.input.mouse_pressed[SDL_BUTTON_RIGHT]) // spawn wheel.
    {
        if (can_spawn_right_mouse)
        {
            body_def.position.x = game.input.mouse_x_meter;
            body_def.position.y = game.input.mouse_y_meter;
            spawn_circle_from_image("gfx/wheel.png", body_def, shape_def);
            can_spawn_right_mouse = false;
        }
    }
    else if (!game.input.mouse_pressed[SDL_BUTTON_RIGHT])
    {
        can_spawn_right_mouse = true;
    }

    if (game.input.keyboard[SDL_SCANCODE_D]) // apply torque with `A` and `D`.
    {
        player_movement_force.x = 1;
        b2Body_SetAngularVelocity(player_entity->body_id, player_movement_force.x);
    }
    else if (game.input.keyboard[SDL_SCANCODE_A])
    {
        player_movement_force.x = -1;
        b2Body_SetAngularVelocity(player_entity->body_id, player_movement_force.x);
    }
    else
    {
        player_movement_force.x = 0;
    }

    b2World_Step(
        world.world_id, // World ID.
        1 / 60.f,       // Time step.
        8,              // Velocity iterations.
        3               // Time iterations.
    );
}

static void game_draw(void)
{
    // Draw a simple shape for the terrain.
    SDL_Colour red = {0xff, 0x00, 0x00, 0xff};
    b2Polygon transformed_polygon;
    b2Vec2 pos;
    float angle;
    for (int i = 0; i < world.n_terrain; ++i)
    {
        const b2Polygon *polygon = b2Shape_GetPolygon(world.p_terrain_shape_id[i]);
        transformed_polygon = b2TransformPolygon(
            b2Body_GetTransform(world.p_terrain_id[i]),
            polygon);
        // Alloc and dealloc the polygon every time. This is pretty inefficient.
        float *print_vert = calloc(2 * polygon->count, sizeof(float));
        for (int j = 0; j < polygon->count; ++j)
        { // There must be a better way.
            b2Vec2 curr = transformed_polygon.vertices[j];
            print_vert[2 * j] = curr.x * PIXEL_PER_METER;
            print_vert[2 * j + 1] = curr.y * PIXEL_PER_METER;
        }
        // Draw the shape under its hitbox for testing.
        if (arrlen(world.p_terrain_data) > 0){ // Hope this condition works.
            uint32_t curr = world.p_terrain_data[i];
            pos = b2Body_GetPosition(world.p_terrain_id[i]);
            angle = b2Body_GetAngle(world.p_terrain_id[i]);
            blit_image(
                game.p_tile_array[curr & ~TILE_FACING_MASK].texture,
                pos.x * PIXEL_PER_METER,
                pos.y * PIXEL_PER_METER,
                angle,
                1);
        }
        GPU_Polygon(game.p_screen, polygon->count, print_vert, red);
        free(print_vert);
    }
    // GPU_RectangleFilled(game.p_screen, 0, game.scaled.h_meter*0.7f - 30, game.scaled.w_meter, game.scaled.h_meter*0.7f, red);
    //  Use a shader for the entites just for its own sake.
    // GPU_ActivateShaderProgram(game.p_shaders[0].shader, &game.p_shaders[0].block);
    // float t = SDL_GetTicks() / 1200.f;
    // float fcolour[4] = {(1 + sin(t) / 2), (1 + sin(t + 1)) / 2, (1 + sin(t + 2)) / 2, 1.0f};
    // GPU_SetUniformfv(game.p_shaders[0].p_res[0], 4, 1, fcolour);
    // Draw all sprites in their current locations.
    PF_Entity *e;
    for (e = world.entity_head.next; e != NULL; e = e->next)
    {
        pos = b2Body_GetPosition(e->body_id);
        angle = b2Body_GetAngle(e->body_id);
        blit_image(
            e->texture,
            pos.x * PIXEL_PER_METER,
            pos.y * PIXEL_PER_METER,
            angle,
            1);
    }
    GPU_ActivateShaderProgram(0, NULL);

    PF_camera_update_camera();
    PF_font_blit_text(game.p_screen, test_font, 40, 40, (SDL_Color){0xff, 0xff, 0xff, 0xff},"Hello, World!");
}
