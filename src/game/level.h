// Logic for the world in gameplay mode.
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

// Init and Cleanup
/*!
 * Initialises the structures et al for gameplay.
 * Adds the cleanup to the appropriate exit queue.
 */
void init_game_world(void);

/*!
 * Cleans up all memory related to gameplay.
 */
void cleanup_game_world(void);

/*!
 * Sets the current application mode to gameplay.
 */
void mode_game_activate(void);

#ifdef __cplusplus
}
#endif
