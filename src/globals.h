// A container for all globals, to allow for easy and unified inclusion.
#include "structs.h"

extern PF_Game game;
extern int run;

extern PF_World world;
