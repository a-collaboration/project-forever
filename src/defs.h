/*!
 * Defines for the main project.
 * Follow the convention of ALL_CAPS_SNAKE_CASE for defines.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

// Startup modes. Determine the state of the programme after initialisation.
enum start_modes{
    START_MODE_GAME,
    START_MODE_EDITOR
};

// Hard-code the native game size. Everything else is scaled.
#define SCREEN_WIDTH 1200
#define SCREEN_HEIGHT 600
// #define SCREEN_WIDTH 1920
// #define SCREEN_HEIGHT 1080

// Let 1m = 32 px at default zoom for box2D world
// this gives the world size 16m x 9m at default zoom
#define PIXEL_PER_METER 32
#define METER_PER_PIXEL 0.03125
// #define SCREEN_WIDTH_METER 60
// #define SCREEN_HEIGHT_METER 33.75
#define SCREEN_WIDTH_METER ((SCREEN_WIDTH) * (METER_PER_PIXEL))
#define SCREEN_HEIGHT_METER ((SCREEN_HEIGHT) * (METER_PER_PIXEL))
// Terrain is composed of tiles. Tiles are 32x32 and target a 1920x1080 internal size.
#define TILE_SIZE 32
#define MAX_TILES 1000 // Make this larger if we ever need more.

#define N_TILES_WIDTH 60
#define N_TILES_HEIGHT 35

// Don't let filenames get too long.
#define MAX_FILENAME_LENGTH 128

// A decent pi.
#define PI 3.14159265358979323846    

#ifdef __cplusplus
}
#endif
