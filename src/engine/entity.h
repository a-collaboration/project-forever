// Logic for spawning entites.
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "../structs.h"

/*!
 * Creates a box based on an image it's pixels. Adds the box to game world and entity linked list.
 * 
 * @param texture_path Path to the image 
 * @param body_def Body definition, position and intial parameters.
 * @param shape_def Shape definition, shape will be overriden by a box defined by image pixels.
 */
PF_Entity* spawn_box_from_image(const char *texture_path,b2BodyDef body_def,b2ShapeDef shape_def);

/*!
 * Creates a circle based on an image it's pixels. Adds the circle to game world and entity linked list.
 * 
 * @param texture_path Path to the image 
 * @param body_def Body definition, position and intial parameters.
 * @param shape_def Shape definition, shape will be overriden by a circle defined by image pixels.
 */
PF_Entity* spawn_circle_from_image(const char *texture_path,b2BodyDef body_def,b2ShapeDef shape_def);

#ifdef __cplusplus
}
#endif
