#include "camera.h"

#include "../globals.h"
#include "../engine/entity.h"
#include "input.h"

static void do_camera_moved(void);
static void do_nothing(void);
static void apply_inverse_attactor(PF_Entity *e, float x_meter, float y_meter, float deadzone, float scalling_factor);

typedef void (*Camera_translation_func)(void);

Camera_translation_func camera_translation_func = do_nothing;
PF_Entity *p_camera_target;
PF_Entity *p_camera_target_attractor;
b2Vec2 camera_attractor_target_delta = {0, 0};

b2Vec2 camera_target_point;
b2Vec2 camera_focus_point;

b2Vec2 camera_lerp_start_point;
Uint64 camera_lerp_duration;
Uint64 camera_lerp_start_time;
Uint64 camera_lerp_current_time;
float camera_lerp_param;
bool camera_finished = true;

float camera_zoom_max = 5.f;
float camera_zoom_min = 0.1f;

float camera_follow_deadzone = 0.5f;
float camera_follow_attractor_multiplier = 1.f;

void PF_camera_update_camera(void)
{
    camera_translation_func();
    GPU_SetCamera(game.p_screen, &game.p_screen->camera);
}

void PF_camera_move_camera(float dx, float dy, float dzoom)
{
    game.p_screen->camera.x += dx;
    game.p_screen->camera.y += dy;
    game.p_screen->camera.zoom_x += dzoom;
    game.p_screen->camera.zoom_y += dzoom;

    if (game.p_screen->camera.zoom_x > camera_zoom_max)
    {
        game.p_screen->camera.zoom_x = camera_zoom_max;
        game.p_screen->camera.zoom_y = camera_zoom_max;
    }
    else if (game.p_screen->camera.zoom_x < camera_zoom_min)
    {
        game.p_screen->camera.zoom_x = camera_zoom_min;
        game.p_screen->camera.zoom_y = camera_zoom_min;
    }

    if (dx != 0 || dy != 0 || dzoom != 0)
    {
        do_camera_moved();
    }
}

void PF_camera_look_at_screen_space(float x_pixels, float y_pixels)
{
    game.p_screen->camera.x = (x_pixels - game.scaled.w / 2) * game.p_screen->camera.zoom_x;
    game.p_screen->camera.y = (y_pixels - game.scaled.h / 2) * game.p_screen->camera.zoom_y;
    do_camera_moved();
}

void PF_camera_look_at(float x_meter, float y_meter)
{
    PF_camera_look_at_screen_space(x_meter * PIXEL_PER_METER, y_meter * PIXEL_PER_METER);
}

/// does nothing for update_camera
static void do_nothing(void)
{
}

void PF_camera_set_nothing(void)
{
    camera_translation_func = do_nothing;
    camera_finished = true;
}

/// @brief called when camera changes. Updates world mouse position and world camera focus point.
static void do_camera_moved(void)
{
    set_pixel_to_world(game.scaled.w / 2, game.scaled.h / 2, &camera_focus_point.x, &camera_focus_point.y);
    update_mouse_position();
}

/// Linear interpolation between start and finish. When param = 1 finish will be returned.
static float lerp(float start, float finish, float param)
{
    return start + param * (finish - start);
}

/// @brief preforms a lerp to a set target, delegated to update_camera.
static void do_lerp(void)
{
    camera_lerp_current_time = SDL_GetTicks64();
    camera_lerp_param = ((float)camera_lerp_current_time - (float)camera_lerp_start_time) / ((float)camera_lerp_duration);

    if (camera_lerp_param > 1.f)
    {
        camera_lerp_param = 1;
        camera_translation_func = do_nothing;
    }

    float current_x = lerp(camera_lerp_start_point.x, camera_target_point.x, camera_lerp_param);
    float current_y = lerp(camera_lerp_start_point.y, camera_target_point.y, camera_lerp_param);
    PF_camera_look_at(current_x, current_y);
}

void PF_camera_set_lerp_target(float x_meter, float y_meter, Uint64 duration)
{
    camera_lerp_start_time = SDL_GetTicks64();
    camera_lerp_duration = duration;
    set_pixel_to_world(game.scaled.w / 2, game.scaled.h / 2, &camera_lerp_start_point.x, &camera_lerp_start_point.y);
    camera_target_point.x = x_meter;
    camera_target_point.y = y_meter;
    camera_translation_func = do_lerp;
    camera_finished = false;
}

/// @brief centers screen on an entity, delegated to update_camera.
static void do_look_at_entity(void)
{
    b2Vec2 pos = b2Body_GetPosition(p_camera_target->body_id);
    PF_camera_look_at(pos.x, pos.y);
}

void PF_camera_set_entity_target(PF_Entity *e)
{
    p_camera_target = e;
    camera_translation_func = do_look_at_entity;
    camera_finished = false;
}

static void do_look_at_entity_apply_attractor(void)
{
    b2Vec2 pos = b2Body_GetPosition(p_camera_target_attractor->body_id);
    apply_inverse_attactor(p_camera_target,
                           pos.x + camera_attractor_target_delta.x,
                           pos.y + camera_attractor_target_delta.y,
                           camera_follow_deadzone,
                           camera_follow_attractor_multiplier);

    pos = b2Body_GetPosition(p_camera_target->body_id);
    PF_camera_look_at(pos.x, pos.y);
}

PF_Entity *PF_camera_follow_anchor_default(b2Vec2 positon, bool enable_collisions, bool visable)
{
    PF_Entity *anchor;
    // Camera target entity
    b2BodyDef b_def = b2_defaultBodyDef;
    b_def.type = b2_dynamicBody;
    b_def.gravityScale = 0;
    b_def.linearDamping = 1;
    // b_def.fixedRotation = true; // cannot rotate
    b_def.position.x = game.scaled.w_meter / 2;
    b_def.position.y = game.scaled.h_meter / 2;
    b2ShapeDef s_def = b2_defaultShapeDef;
    s_def.density = 10;
    if (!enable_collisions)
    {
        s_def.filter.categoryBits = 0; // remove collision
    }

    if (visable)
    {
        anchor = spawn_circle_from_image("gfx/camera.png", b_def, s_def);
    }
    else
    {
        anchor = spawn_circle_from_image("gfx/camera_empty.png", b_def, s_def);
    }
    return anchor;
}

PF_Entity *PF_camera_follow_init(PF_Entity *target, PF_Entity *camera_entity)
{
    p_camera_target = camera_entity;
    p_camera_target_attractor = target;
    camera_translation_func = do_look_at_entity_apply_attractor;
    camera_finished = false;

    return camera_entity;
}

void PF_camera_follow_target_shift(float dx, float dy)
{
    camera_attractor_target_delta.x = dx;
    camera_attractor_target_delta.y = dy;
}

/// @brief Apply an attracting force to an Entity towards a coordinate. Magnitude of force proportial to difference.
/// @param e Entity that is being attracted.
/// @param x_meter x position of attractor source.
/// @param y_meter y position og attractor source.
/// @param deadzone when object in deadzone velocity is set to zero.
/// @param scalling_factor an additonal parameter to scale the attraction force's magnitude.
static void apply_inverse_attactor(PF_Entity *e, float x_meter, float y_meter, float deadzone, float scalling_factor)
{
    b2Vec2 world_position = b2Body_GetPosition(e->body_id);
    b2Vec2 force;
    b2Vec2 zero = {0, 0};
    b2Vec2 target = {x_meter, y_meter};

    force.x = target.x - world_position.x;
    force.y = target.y - world_position.y;

    float magnitude = sqrtf(force.x * force.x + force.y * force.y);
    if (magnitude < deadzone)
    {
        b2Body_SetLinearVelocity(e->body_id, zero);
    }
    else if (force.x != 0 && force.y != 0)
    {
        force.x = force.x * magnitude * scalling_factor;
        force.y = force.y * magnitude * scalling_factor;

        b2Body_ApplyForceToCenter(e->body_id, force, true);
    }
}
