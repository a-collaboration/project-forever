#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * Grabs the SDL inputs and handles them appropriately.
 * Input events might be relevant to the game and might at some point 
 * in the future be passed through to something like IMGUI too.
 *
 * Window events allow for automated resizing through some basic maths with viewports.
 */
void handle_input(void);


/*!
 * Update the mouse world positon. Called automatically whenever the mouse moves.
 * This should be called explicitally whenever the camera is moved and 
 * the mouse is not.
 */
void update_mouse_position(void);

void set_pixel_to_world(float pixel_x, float pixel_y, float *word_x, float *world_y);

#ifdef __cplusplus
}
#endif
