#pragma once
// C++ header guards.
#ifdef __cplusplus
extern "C" {
#endif

// Initialisers for various systems - graphics, input, audio, etc.
// Also covers cleanup functions - the alpha and omega.

/*!
 * Primary initialisation function. Calls all others in turn.
 * @param title An initial title for the programme window.
 */
void init(const char*);


/*!
 * Primary cleanup function. Calls all others in turn.
 */
void cleanup(void);

/*!
 * Push an item to a deletion stack. Marks it for cleanup at exit, in a FILO order.
 * Neatly allows things to be cleaned up before their dependencies.
 */
typedef struct PF_DelStack PF_DelStack;
void push_deletion(void (*)(void));

/*!
 * Flush a deletion stack. FILO, as expected.
 * Iterates over the stack, calling each deletion function in turn.
 */
void flush_deletion_stack(void);

#ifdef __cplusplus
}
#endif
