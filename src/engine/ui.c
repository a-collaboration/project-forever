#include "ui.h"
#include "../globals.h"
#include "init.h"
#include "text.h"

#include <SDL2/SDL_gpu.h>
#include <microui/microui.h>
#include <microui/atlas.inl>
#include <stb/stb_ds.h>
#include <string.h>

static void cleanup_ui(void);
static int text_width(mu_Font, const char*, int);
static int text_height(mu_Font);
static GPU_Image* make_icon_atlas(void);

static PF_Font *menu_font = NULL;
static GPU_Image *mu_atlas = NULL;

void init_ui(void)
{
    menu_font = PF_font_open(game.p_screen, "fnt/CharisSil-Jw77.ttf", 24);
    game.p_ctx = malloc(sizeof(mu_Context));
    game.using_ui = 1;
    mu_init(game.p_ctx);
    game.p_ctx->text_width = text_width;
    game.p_ctx->text_height = text_height;
    // Assemble the default atlas.
    mu_atlas = make_icon_atlas();
    push_deletion(cleanup_ui);
}

static void cleanup_ui(void)
{
    PF_font_close(menu_font);
    free(game.p_ctx);
    GPU_FreeImage(mu_atlas);
    game.using_ui = 0;
}

static int text_width(mu_Font font, const char* text, int len)
{
    return PF_font_measure_text(menu_font, text);
}

static int text_height(mu_Font font)
{
    // This is adjusted to offset drawing from centre rather than top left.
    return -0.5 * PF_font_text_height(menu_font);
}

static GPU_Image* make_icon_atlas(void)
{
    GPU_Image *texture = GPU_CreateImage(ATLAS_WIDTH, ATLAS_HEIGHT, GPU_FORMAT_RGBA);
    // The atlas provides the text in GL_ALPHA format: A8 only, unsupported by modern GL.
    unsigned char write_bytes[4 * ATLAS_WIDTH * ATLAS_HEIGHT];
    for (int i = 0; i < 4 * ATLAS_WIDTH * ATLAS_HEIGHT; i+=4)
    {
        for (int j = 0; j < 3; ++j)
        {
            write_bytes[i+j] = 0xff;
        }
        write_bytes[i+3] = atlas_texture[i/4];
    }
    GPU_UpdateImageBytes(texture, NULL, write_bytes,  ATLAS_WIDTH*4);
    return (texture);
}

// An example UI element.
static void test_ui(void)
{
    if (mu_begin_window(game.p_ctx, "Test Window", mu_rect(40, 40, 350, 350)))
    {
        mu_layout_row(game.p_ctx, 1, (int[]){-1}, -25);
        mu_begin_panel(game.p_ctx, "A panel");
        mu_Container *panel = mu_get_current_container(game.p_ctx);
        mu_layout_row(game.p_ctx, 1, (int[]){-1}, -1);
        mu_text(game.p_ctx, "Hello");
        mu_end_panel(game.p_ctx);
        mu_end_window(game.p_ctx);
    }
}

// Zero out the game inputs when using the UI.
// There is almost certainly a better way to do this. Look into it later.
// Maybe even just integrate this at the input level.
static void capture_inputs(void)
{
    if (game.p_ctx->focus)
    {
        memset(&game.input, 0, sizeof(struct Input));
    }
}

void process_ui(int n, void (**functions)(void))
{
    mu_begin(game.p_ctx);
    // Handle the UI things relevant to your current mode.
    //test_ui();
    // Run the presented UI functions.
    for (int i = 0; i < n; ++i)
    {
        functions[i]();
    }
    // Finally, ensure that inputs aren't processed by the rest of the process.
    capture_inputs();
    mu_end(game.p_ctx);
}

void draw_ui(void)
{
    mu_Command *cmd = NULL;
    while (mu_next_command(game.p_ctx, &cmd)) {
        switch(cmd->type) {
            case MU_COMMAND_TEXT:
                PF_font_blit_text(
                        game.p_screen,
                        menu_font,
                        cmd->text.pos.x,
                        cmd->text.pos.y,
                        (SDL_Color){
                        cmd->text.color.r,
                        cmd->text.color.g,
                        cmd->text.color.b,
                        cmd->text.color.a
                        },
                        cmd->text.str
                        );
                break;
            case MU_COMMAND_RECT:
                GPU_RectangleFilled(
                        game.p_screen,
                        cmd->rect.rect.x,
                        cmd->rect.rect.y,
                        cmd->rect.rect.x+cmd->rect.rect.w,
                        cmd->rect.rect.y+cmd->rect.rect.h,
                        (SDL_Color){
                        cmd->rect.color.r,
                        cmd->rect.color.g,
                        cmd->rect.color.b,
                        cmd->rect.color.a
                        }
                        );
                break;
            case MU_COMMAND_ICON:;
                // Compile an image from the pixels provided.
                // Blit to the centre to keep icons at base scale.
                mu_Rect mu_src = atlas[cmd->icon.id];
                GPU_Rect src = {
                    .x = mu_src.x,
                    .y = mu_src.y,
                    .w = mu_src.w,
                    .h = mu_src.h
                };
                GPU_Rect dst = {
                    .x = cmd->icon.rect.x,
                    .y = cmd->icon.rect.y,
                    .w = cmd->icon.rect.w,
                    .h = cmd->icon.rect.h
                };
                GPU_Blit(
                        mu_atlas,
                        &src,
                        game.p_screen,
                        dst.x+0.5*dst.w,
                        dst.y+0.5*dst.h
                        );
                break;
            case MU_COMMAND_IMAGE:;
                PF_AtlasImage *img = (PF_AtlasImage*)cmd->image.img;
                GPU_Blit(
                        img->texture,
                        &img->rect,
                        game.p_screen,
                        cmd->image.dst.x + 0.5*cmd->image.dst.w,
                        cmd->image.dst.y + 0.5*cmd->image.dst.h
                        );
                break;
        }
    }
}

/* Custom UI Elements */
static mu_Rect unclipped_rect = { 0, 0, 0x1000000, 0x1000000 };

void mu_draw_image(mu_Context* ctx, const char* key, mu_Rect dst)
{
    PF_AtlasImage *img = &shget(game.p_sprite_atlas, key);
    mu_Command *cmd;
    /* Issue a clip command if the rect isn't fully contained within the cliprect. */
    int clipped = mu_check_clip(game.p_ctx, dst);
    if (clipped == MU_CLIP_ALL) { return; }
    if (clipped == MU_CLIP_PART) { mu_set_clip(game.p_ctx, mu_get_clip_rect(game.p_ctx)); }
    /* Do image command. */
    cmd = mu_push_command(game.p_ctx, MU_COMMAND_IMAGE, sizeof(mu_ImgCommand));
    cmd->image.dst = dst;
    cmd->image.img = (void*)img;
    /* Reset clipping if applicable. */
    if (clipped) { mu_set_clip(game.p_ctx, unclipped_rect); }
}

int mu_picture_toggle_ex(mu_Context* ctx, const char* filename, int icon, int opt)
{
    int res = 0;
    mu_Id id = mu_get_id(game.p_ctx, filename, strlen(filename));
    PF_AtlasImage *img = &shget(game.p_sprite_atlas, filename);
    mu_layout_height(game.p_ctx, img->rect.h+1);
    mu_layout_width(game.p_ctx, img->rect.w+1);
    mu_Rect r = mu_layout_next(game.p_ctx);
    mu_update_control(game.p_ctx, id, r, opt);
    /* Handle click. */
    if (ctx->mouse_pressed == MU_MOUSE_LEFT && ctx->focus == id)
    {
        res |= MU_RES_SUBMIT;
    }
    /* Draw */
    mu_draw_control_frame(ctx, id, r, MU_COLOR_PANELBG, opt);
    mu_draw_image(ctx, filename, r);
    if (icon) { mu_draw_icon(ctx, icon, r, ctx->style->colors[MU_COLOR_TEXT]); }
    return res;
}

