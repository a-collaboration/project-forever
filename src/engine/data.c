#include <stdio.h>
#include <string.h>
#include <zlib.h>

#include "../globals.h"
#include "data.h"


/* IO Functions */
/* Writing-side components of a save. */
#define WRITE_MAP_FAIL(str) \
    printf(str); \
    free(comp_array); \
    free(output); \
    return 0

static _Bool write_map(FILE *fp, const int w, const int h, const uint32_t *arr)
{
    // Prepend the width and height (cast to uint32_t) to the tiles and compress.
    const uint32_t len = (w * h) + 2;
    const uint32_t byte_len = sizeof(uint32_t) * len;
    uint32_t *map_arr = malloc(byte_len);

    map_arr[0] = (uint32_t)w;
    map_arr[1] = (uint32_t)h;
    memcpy(map_arr + 2, arr, byte_len-8);

    uint32_t comp_len = sizeof(char)*compressBound((uLong)byte_len);
    unsigned char *comp_array = (unsigned char *)map_arr;
    unsigned char *output = malloc(comp_len);

    if (!compress_bytes(comp_array, byte_len, output, &comp_len)) {
        WRITE_MAP_FAIL("Error compressing tile data.\n");
    }
    // With the data compressed, write the chunk.
    // Length (uint32_t):
    if (fwrite(&comp_len, sizeof(uint32_t), 1, fp) != 1){
        WRITE_MAP_FAIL("Error writing map length\n");
    }
    // Original Length (uint32_t);
    if (fwrite(&byte_len, sizeof(uint32_t), 1, fp) != 1){
        WRITE_MAP_FAIL("Error writing original length.");
    }
    // Chunk Type: (char[4]):
    const char chunk_type[] = {'T', 'I', 'L', 'E'};
    if (fwrite(chunk_type, sizeof(char), 4, fp) != 4) {
        WRITE_MAP_FAIL("Error writing map type.\n");
    }
    // Chunk Data (compressed bytes):
    if (fwrite(output, sizeof(char), comp_len, fp) != comp_len) {
        WRITE_MAP_FAIL("Error writing map data.\n");
    }
    // CRC/Checksum:
    uint32_t checksum = crc32(0, output, comp_len);
    if (fwrite(&checksum, sizeof(uint32_t), 1, fp) != 1) {
        WRITE_MAP_FAIL("Error writing map checksum.\n");
    }

    free(output);
    free(comp_array);
    return 1;
}

#define WRITE_CHECK(x) if (!x) { goto write_error; }

_Bool save_map(const char *path, const char *fmt, ...)
{
    // Open the file and write to it.
    FILE *fp = fopen(path, "wb");
    if (fp == NULL) {
        printf("Unable to open file to save: %s\n", path);
        return 0; // Don't modify an array and return a failure.
    }
    // First, write the signature.
    // Below throughout, we use gotos for error handling.
    unsigned char signature[2] = {0x50, 0x46};
    if (fwrite(signature, sizeof(char), 2, fp) != 2)
    {
        printf("Error writing file signature. Something is wrong.\n");
        goto write_error; 
    }
    // Next, handle the chunks.
    // Note that `va_arg` in function arguments can cause problems.
    va_list ap;
    va_start(ap, fmt);
    char c;
    for (int i = 0; (c = fmt[i]) != 0; ++i)
    {
        switch (c) {
            case 'm':;
                const int w = va_arg(ap, const int);
                const int h = va_arg(ap, const int);
                const uint32_t * arr = va_arg(ap, const uint32_t *);
                WRITE_CHECK(write_map(fp, w, h, arr));
                break;
            default:
                break;
        }
    }
    va_end(ap);
    fclose(fp);
    return 1;
write_error:
    fclose(fp);
    return 0;
}

/* Reading-side components of a save. */
#define READ_FAIL(x) \
    printf(x); \
    fclose(fp); \
    return data;

PF_SaveData load_map(const char *path)
{
    PF_SaveData data = {};
    // Open the file for reading.
    FILE *fp = fopen(path, "rb");
    if (fp == NULL) {
        printf("Unable to open file to load: %s\n", path);
        return data;
    }
    // Get the file length.
    fseek(fp, 0, SEEK_END);
    size_t file_len = ftell(fp);
    rewind(fp);

    // First validate the signature.
    unsigned char signature[2];
    if (fread(signature, sizeof(char), 2, fp) != 2){
        READ_FAIL("Unable to read file signature.\n");
    }
    if (*(short*)signature != 0x4650) { // Reinterpret the bytes of '0x50 0x46'.
        READ_FAIL("Unexpected file signature. Possible corruption.\n");
    }
    // If the signature if verified, then read the arbitrary remaining blocks.
    while(ftell(fp) < file_len)
    {
        uint32_t chunk_len, chunk_full_len, chunk_type, chunk_sum;
        // Read the next chunk length.
        if (fread(&chunk_len, sizeof(uint32_t), 1, fp) != 1) {
            READ_FAIL("Failed to read chunk length.\n");
        }
        // Read the origignal length.
        if (fread(&chunk_full_len, sizeof(uint32_t), 1, fp) != 1) {
            READ_FAIL("Failed to read original chunk length.\n");
        }
        // Read the chunk type.
        if (fread(&chunk_type, sizeof(uint32_t), 1, fp) != 1) {
            READ_FAIL("Failed to read chunk type.\n");
        }
        // Read the compressed bytes.
        unsigned char *chunk_bytes = malloc(sizeof(char) * chunk_len);
        if (fread(chunk_bytes, sizeof(char), chunk_len, fp) != chunk_len) {
            free(chunk_bytes);
            READ_FAIL("Failed to read chunk.\n");
        }
        // Read the checksum.
        if (fread(&chunk_sum, sizeof(uint32_t), 1, fp) != 1) {
            free(chunk_bytes);
            READ_FAIL("Failed to read chunk checksum.\n");
        }
        // With the data read, now compute the CRC checksum on the chunk bytes.
        uLong checksum = crc32(0, chunk_bytes, chunk_len);
        if (chunk_sum != checksum) {
            printf("Chunk checksum does not match records: 0x%zu 0x%u\n", checksum, chunk_sum);
            free(chunk_bytes);
            continue; // Try to head to the next chunk. We've already read all of it.
        }

        // We've confirmed the checksum, so decompress the data before processing.
        unsigned char *decompressed_bytes = malloc(sizeof(char) * chunk_full_len);
        if (!decompress_bytes(chunk_bytes, chunk_len, decompressed_bytes, &chunk_full_len)) {
            printf("Failed to decompress chunk. Decompressed %u bytes.\n", chunk_full_len);
            free(decompressed_bytes);
            free(chunk_bytes);
            continue;
        }

        // Now process the chunk according to its type.
        switch (chunk_type)
        {
            case 0x454c4954: // 'TILE'
                printf("Loading tile map.\n");
                uint32_t *tile_arr = (uint32_t*)decompressed_bytes;
                // The first two bytes are the height and width.
                data.tile_width = tile_arr[0];
                data.tile_height = tile_arr[1];
                uint32_t tile_arr_len = data.tile_width * data.tile_height * sizeof(uint32_t);
                data.tile_arr = malloc(tile_arr_len);
                memcpy(data.tile_arr, tile_arr+2, tile_arr_len);
                break;
            default:
                printf("Unrecognised chunk type: 0x%x\n", chunk_sum);
                break;
        }

        free(decompressed_bytes);
        free(chunk_bytes);
        printf("Load progress: %zu / %zu\n", ftell(fp), file_len);
    }

    fclose(fp);
    return data;
}

void free_save_data(struct PF_SaveData *data)
{
   if (data->tile_arr) { free(data->tile_arr); }
}

_Bool compress_bytes(const unsigned char *input, uint32_t length_in, unsigned char *output, uint32_t *length_out)
{
    // Set up the stream.
    z_stream stream;
    stream.zalloc = Z_NULL;
    stream.zfree = Z_NULL;
    stream.opaque = Z_NULL;

    // Init.
    if (deflateInit(&stream, Z_DEFAULT_COMPRESSION) != Z_OK) { return 0; }

    // Set the I/O.
    stream.next_in = (Bytef*)input;
    stream.avail_in = length_in;
    stream.next_out = (Bytef*)output;
    stream.avail_out = *length_out;

    // Do the compression.
    if (deflate(&stream, Z_FINISH) != Z_STREAM_END)
    {
        deflateEnd(&stream);
        return 0;
    }
    
    // Conclude.
    *length_out = stream.total_out;
    if (deflateEnd(&stream) != Z_OK) { return 0; }

    return 1;
}


_Bool decompress_bytes(const unsigned char *input, uint32_t length_in, unsigned char *output, uint32_t *length_out)
{
    // Set up the stream.
    z_stream stream;
    stream.zalloc = Z_NULL;
    stream.zfree = Z_NULL;
    stream.opaque = Z_NULL;
 
    // Init.
    if (inflateInit(&stream) != Z_OK) { return 0; }

    // Set the I/O.
    stream.next_in = (Bytef*)input;
    stream.avail_in = length_in;
    stream.next_out = (Bytef*)output;
    stream.avail_out = *length_out;

    // Do the decompression.
    if (inflate(&stream, Z_FINISH) != Z_STREAM_END)
    {
        inflateEnd(&stream);
        return 0;
    }
    
    // Conclude.
    *length_out = stream.total_out;
    if (inflateEnd(&stream) != Z_OK) { return 0; }

    return 1;
}
