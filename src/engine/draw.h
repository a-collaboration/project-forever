#pragma once

#ifdef __cplusplus
extern "C" {
#endif

// #include "../../libs/SDL_gpu/include/SDL_gpu.h"
#include <SDL2/SDL_gpu.h>
#include <stdint.h> // For uint32_t.

/*!
 * Initialises the graphical components of the elements.
 * As this initialises SDL, call this BEFORE other SDL modules.
 */
void init_graphics(const char*);

/*!
 * Initialises the sprite atlas.
 * Loads the sprite atlas and metadata from the `dat` dir.
 */
void init_atlas(void);

/*!
 * Initialises the shader pipelines used by the programme.
 */
void init_shaders(void);

/*!
 * Provides functionality to free the data associated with the sprite atlas.
 */
void cleanup_atlas(void);

/*!
 * Provides functionality to free the shader data.
 */
void cleanup_shaders(void);

/*!
 * Clears the screen to a basic grey clear colour.
 */
void prepare_scene(void);

/*!
 * Performs the flip and presents the draw buffer.
 */
void present_scene(void);

/*!
 * A very basic draw function - just blits the texture from a sprite atlas image.
 * @param img The image from the sprite atlas to blit.
 * @param x The X co-ordinate at which to centre the image.
 * @param y The Y co-ordinate at which to centre the image.
 * @param angle The angle to which to rotate the image.
 * @param scale The factor by which to scale the image. This could be broadened to a scale_x and scale_y.
 */
typedef struct PF_AtlasImage PF_AtlasImage;
void blit_image(PF_AtlasImage*, float, float, float, float);

/*!
 * Load a texture from a file into a GPU_Image.
 */
GPU_Image* load_texture(const char *);

/*!
 * Compile a vertex and fragment shader into a shader block.
 * @param p Index of the linked shader.
 * @param vertex_shader_file Path to the vertex shader in `shd`.
 * @param fragment_shader_file Path to the fragment shader in `shd`.
 * @return the compiled shader block.
 */
GPU_ShaderBlock load_shader_block(uint32_t *, const char *, const char *);

/*!
 * Shaders
 * The shaders available to the programme. Each works as a nice, readable index
 * and can so be used to reference and activate the shader.
 */
enum shaders {
    SHADER_COLOURS,
    SHADER_OPACITY,
    SHADER_MAX
};

/*!
 * Set the active shader.
 * All of the targets should have been initialised at startup.
 */
void activate_shader(enum shaders);

#ifdef __cplusplus
}
#endif
