#include "tiles.h"
#include "globals.h"
#include "init.h"

#include <box2d/box2d.h>
#include <box2d/geometry.h>
#include <box2d/hull.h>
#include <box2d/types.h>
#include <jansson.h>
#include <stb/stb_ds.h>

static void cleanup_tiles(void);

void init_tiles(void)
{
    // Read in tile data from a JSON array.
    json_t *tile_data, *node;
    json_error_t error;
    tile_data = json_load_file("dat/tiles.json", 0, &error);
    if (!tile_data || !json_is_array(tile_data))
    {
        printf("Something happened: %s at %s.\n", error.text, error.source);
    }
    // Read tiles into a global array at startup.
    game.p_tile_array = calloc(json_array_size(tile_data), sizeof(PF_Tile));
    size_t index;
    json_array_foreach(tile_data, index, node)
    {
        if (!json_is_object(node))
        {
            printf("Something went wrong with node %zu of tiles.json\n", index);
            continue;
        }
        strncpy(game.p_tile_array[index].label,
                json_string_value(json_object_get(node, "label")),
                MAX_FILENAME_LENGTH);
        const char *filename = json_string_value(json_object_get(node, "texture"));
        if (filename != NULL) {
            game.p_tile_array[index].texture = &shget(
                    game.p_sprite_atlas,
                    json_string_value(json_object_get(node, "texture"))
                    );
        }
        game.p_tile_array[index].hitbox = json_integer_value(json_object_get(node, "hitbox"));
    }
    game.n_tiles = index;
    json_decref(tile_data);
    // Tidy tiles at the end of it.
    push_deletion(cleanup_tiles);
}

static void cleanup_tiles(void)
{
    free(game.p_tile_array);
    game.n_tiles = 0;
}

/* Have fun. */
b2Polygon get_tile_hitbox(uint32_t tile_data)
{
    enum tile_structure s = tile_data & ~TILE_FACING_MASK;
    switch (s) {
        case TILE_STR_FULL:
        default:
            return b2MakeBox(0.5, 0.5);
            break;
        case TILE_STR_LOW:
            return b2MakeOffsetBox(
                    0.5,
                    0.125,
                    (b2Vec2){0, 0.375},
                    0);
            break;
        case TILE_STR_MED:
            return b2MakeOffsetBox(
                    0.5,
                    0.25,
                    (b2Vec2){0, 0.25},
                    0);
            break;
        case TILE_STR_HIGH:
            return b2MakeOffsetBox(
                    0.5,
                    0.375,
                    (b2Vec2){0, 0.125},
                    0);
            break;
        case TILE_STR_SF_A:
            return b2MakePolygon(
                    &(b2Hull){ .points = {
                    (b2Vec2){-0.5, 0.5},
                    (b2Vec2){0.5, -0.5},
                    (b2Vec2){0.5, 0.5}},
                    .count = 3
                    },
                    0);
            break;
        case TILE_STR_SF_D:
            return b2MakePolygon(
                    &(b2Hull){ .points = {
                    (b2Vec2){-0.5, -0.5},
                    (b2Vec2){0.5, 0.5},
                    (b2Vec2){-0.5, 0.5}},
                    .count = 3
                    },
                    0);
            break;
        case TILE_STR_SH_LOW_A:
            return b2MakePolygon(
                    &(b2Hull){ .points = {
                    {-0.5, 0.5},
                    {0.5, 0},
                    {0.5, 0.5}},
                    .count = 3
                    }, 
                    0);
            break;
        case TILE_STR_SH_LOW_D:
            return b2MakePolygon(
                    &(b2Hull){ .points = {
                    {-0.5, 0.5},
                    {-0.5, 0},
                    {0.5, 0.5}},
                    .count = 3
                    }, 
                    0);
            break;
        case TILE_STR_SH_HIGH_A:
            return b2MakePolygon(
                    &(b2Hull){ .points = {
                    (b2Vec2){-0.5, 0},
                    (b2Vec2){0.5, -0.5},
                    (b2Vec2){0.5, 0.5},
                    (b2Vec2){-0.5, 0.5}},
                    .count = 4
                    }, 
                    0);
            break;
        case TILE_STR_SH_HIGH_D:
            return b2MakePolygon(
                    &(b2Hull){ .points = {
                    (b2Vec2){-0.5, -0.5},
                    (b2Vec2){0.5, 0},
                    (b2Vec2){0.5, 0.5},
                    (b2Vec2){-0.5, 0.5}},
                    .count = 4
                    }, 
                    0);
            break;
    }
}
