#include "text.h"
#include "../globals.h"
#include <SDL2/SDL_gpu.h>
#include <SDL_pixels.h>
#include <SDL_rwops.h>
#include <SDL_stdinc.h>

// Include and build the STB text functions here.
#define STB_RECT_PACK_IMPLEMENTATION
#define STB_TRUETYPE_IMPLEMENTATION
#include <stb/stb_rect_pack.h>
#include <stb/stb_truetype.h>

typedef struct PF_Font {
    stbtt_fontinfo* info;
    stbtt_packedchar * chars;
    GPU_Image *atlas;
    int texture_size;
    float size;
    float scale;
    int ascent;
    int baseline;
} PF_Font;



// Implementation here largely follows the `stbttf` below.
// https://gist.github.com/benob/92ee64d9ffcaa5d3be95edbf4ded55f2

void PF_font_close(PF_Font* font)
{
    if (font->atlas) GPU_FreeImage(font->atlas);
    if (font->info) free(font->info);
    if (font->chars) free(font->chars);
    free(font);
}

/*!
 * PF_font_open serves as a wwrapper around this.
 * Open a TTF font with an SDL IO handler, for a given GPU target and font size.
 * Returns NULL on failure. Creates a texture atlas.
 * @param target The GPU target to use.
 * @param rw The SDL IO handler to use.
 * @param size The font size to use.
 */
static PF_Font* open_font_RW(GPU_Target *target, SDL_RWops *rw, float size)
{
    Sint64 file_size = SDL_RWsize(rw);
    unsigned char *buffer = malloc(file_size);
    if (SDL_RWread(rw, buffer, file_size, 1) != 1) return NULL;
    SDL_RWclose(rw);

    PF_Font *font = calloc(1, sizeof(PF_Font));
    font->info = malloc(sizeof(stbtt_fontinfo));
    font->chars = malloc(sizeof(stbtt_packedchar)*96);

    if (stbtt_InitFont(font->info, buffer, 0) == 0) {
        free(buffer);
        PF_font_close(font);
        return NULL;
    }
    // Create the bitmap atlas of packed characters.
    unsigned char *bitmap = NULL;
    font->texture_size = 32;
    while (true) {
        bitmap = malloc(font->texture_size * font->texture_size);
        stbtt_pack_context pack_context;
        stbtt_PackBegin(&pack_context, bitmap, font->texture_size, font->texture_size, 0, 1, 0);
        stbtt_PackSetOversampling(&pack_context, 2, 2);
        if (!stbtt_PackFontRange( &pack_context, buffer, 0, size, 32, 95, font->chars)) {
            // Too small.
            free(bitmap);
            stbtt_PackEnd(&pack_context);
            font->texture_size *= 2;
        } else {
            stbtt_PackEnd(&pack_context);
            break;
        }
    }
    
    // Convert bitmap to texture.
    font->atlas = GPU_CreateImage(font->texture_size, font->texture_size, GPU_FORMAT_RGBA);
    GPU_SetBlendMode(font->atlas, GPU_BLEND_NORMAL); // Probably not required.

    // Here be dragons. This doesn't map perfectly to the base SDL equivalents.
    unsigned char *pixels = malloc(font->texture_size * font->texture_size * sizeof(Uint32)); // Dangerous but accurate.
    static SDL_PixelFormat *format = NULL;
    if (format == NULL) format = SDL_AllocFormat(SDL_PIXELFORMAT_RGBA32);
    for (int i = 0; i < 4 * font->texture_size * font->texture_size; i+=4) {
        for (int j = 0; j < 3; ++j) {
            pixels[i+j] = 0xff;
        }
        pixels[i+3] = bitmap[i/4];
    }
    GPU_UpdateImageBytes(font->atlas, NULL, pixels, font->texture_size*sizeof(Uint32));
    free(pixels);
    free(bitmap);

    // Additional  setup.
    font->scale = stbtt_ScaleForPixelHeight(font->info, size);
    stbtt_GetFontVMetrics(font->info, &font->ascent, 0, 0);
    font->baseline = (int)(font->ascent * font->scale);

    free(buffer);

    return font;
}

PF_Font* PF_font_open(GPU_Target* target, const char* filename, float size)
{
    SDL_RWops *rw = SDL_RWFromFile(filename, "rb");
    if (rw == NULL) return NULL;
    return open_font_RW(target, rw, size);
}

void PF_font_blit_text(GPU_Target* target, PF_Font* font, float x, float y, SDL_Color colour, const char* text)
{
    GPU_SetRGBA(font->atlas, colour.r, colour.g, colour.b, colour.a);
    for (int i = 0; text[i]; ++i) {
        if (text[i] >= 32) {
            stbtt_packedchar *info = &font->chars[text[i]-32];
            GPU_Rect src_rect = {info->x0, info->y0, info->x1 - info->x0, info->y1 - info->y0};
            GPU_Rect dst_rect = {x + info->xoff, y + info->yoff, info->xoff2 - info->xoff, info->yoff2 - info->yoff};
            GPU_BlitRect(font->atlas, &src_rect, target, &dst_rect);
            x += info->xadvance;
        }
    }
}

float PF_font_measure_text(PF_Font* font, const char* text)
{
    float width = 0;
    for (int i = 0; text[i]; ++i) {
        if (text[i] >= 32) {
            stbtt_packedchar *info = &font->chars[text[i] -32];
            width += info->xadvance;
        }
    }
    return width;
}

float PF_font_text_height(PF_Font *font)
{
    return font->baseline;
}
