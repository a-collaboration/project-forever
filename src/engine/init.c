#include "init.h"
#include "../structs.h"
#include "draw.h"
#include "tiles.h"

#include "../globals.h"

void init(const char* title)
{
    // Initialise graphics and open the window.
    init_graphics(title);
    // Load the sprite atlas. This only depends on SDL_INIT_VIDEO.
    init_atlas();
    // Load the shader blocks/render pipelines.
    init_shaders();
    // Load some game data.
    init_tiles();
}

void cleanup()
{
    // Free the things we want to free, in order.
    flush_deletion_stack();
}

void push_deletion(void (*fun)(void))
{
    // This is a very simple singly linked list. Just add the function to the list.
    PF_DelStack *new_item = malloc(sizeof(PF_DelStack));
    new_item->fun = fun;
    new_item->p_next = game.p_deletion_stack;
    game.p_deletion_stack = new_item;
}

void flush_deletion_stack()
{
    PF_DelStack *current;
    while ((current = game.p_deletion_stack) != NULL) {
        current->fun();
        game.p_deletion_stack = game.p_deletion_stack->p_next;
        free(current);
    }
}
