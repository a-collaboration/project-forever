/*!
 * # Game Data
 * */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif 

#include <stdint.h> 

/*!
 * @defgroup Saving and Loading
 * @{
 */

/*!
 * # Level File Specification
 * Level files are binary data, conventionally stored with a .pfm extension.
 * The data structure is inspired by the PNG format, separating the data into 
 * chunks, each of which contain their own metadata, allowing for extensibility
 * and relatively straightforward parsing.
 * Following the PNG example, each chunk consists of the following components:
 *  - Length of the chunk data in bytes (4 bytes/1 uint32_t).
 *  - Uncompressed length of the data in bytes (4 bytes/1 uint32_t). Not sure how PNG handles the INFLATE.
 *  - Chunk type enum. (4 bytes/1 uint32_t).
 *  - Chunk data (n bytes, specified above by length).
 *  - Checksum (4 bytes/1 uint32_t/1 uLong). This uses the CRC32 checksum of the _compressed_ data.
 * As with PNG, the data is DEFLATE-compressed, although there is no pre-filtering.
 * The level file consists of the following:
 * - File signature: 0x50 0x46 - Encodes "PF" in ASCII for readability.
 * - Terrain Chunk: Tiles associated with the level.
 * - Entity Chunk: Entity data associated with the level.
 */

struct PF_SaveData {
    // Various data that can be loaded from a save file. This is intended to be 
    // populated by the loading functionality from data.h.
    uint32_t tile_width;
    uint32_t tile_height;
    uint32_t *tile_arr;
};

/*!
 * @title Save Map
 * Saves the specified map array into the standardised binary format at the
 * provided path.
 * Possible entries for the format string:
 * - 'm': Map.
 * @param path - The file path to use.
 * @param fmt - The format for the varargs - exactly which components are being saved and in which order.
 */
_Bool save_map(const char *, const char *, ...);

/*!
 * @title Load Map
 * Loads the map from the provided path into the provided array. Does its 
 * own allocation.
 * @param map - The output array. This will have memory allocated to it.
 * @param path - The file path to use.
 * @return A PF_SaveData of the loaded data.
 */
struct PF_SaveData load_map(const char *);

/*!
 * @title Free Save Data
 * Safely frees the memory associated with a `PF_SaveData` struct.
 * @param A pointer to the struct. Note that the struct itself will not be freed.
 */
void free_save_data(struct PF_SaveData *);

/*! @} */

/*!
 * @defgroup Compression
 * @{
 */

/*!
 * @title Compress Bytes
 * Takes in a vector of bytes and applies DEFLATE compression.
 * @param input The bytes to compress.
 * @param length_in The length in bytes of the input.
 * @param output The vector to which to write the output.
 * @param length_out The length of the output.
 */
_Bool compress_bytes(const unsigned char *, uint32_t, unsigned char *, uint32_t *);

/*!
 * @title Decompress Bytes
 * Takes in a vector of bytes and applies INFLATE decompression.
 * @param input The bytes to compress.
 * @param length_in The length in bytes of the input.
 * @param output The vector to which to write the output.
 * @param length_out The length of the output.
 */
_Bool decompress_bytes(const unsigned char *, uint32_t, unsigned char *, uint32_t *);

/*!
 * @}
 */


#ifdef __cplusplus
}
#endif
