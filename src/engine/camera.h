#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <SDL2/SDL.h>
#include "../structs.h"

/**
 * \defgroup Camera 
 * These functions allow you to control the camera's movement.
 * By default the functions work on world co-ordinates in meters, consitent with box2D's world.
 * Transfomrations in screen space are explicitly shown with _screen_space.
 * Functions with set_ indicate that it delegates functionallity to be preformed over multiple frames, progressed with update fucntion.
 * @see PF_camera_update_camera
 * @{
 */

/// @brief Adjust the camera by various deltas.
/// @param dx change in x meters.
/// @param dy change in y meters.
/// @param dzoom change in zoom multiplier.
void PF_camera_move_camera(float dx, float dy, float dzoom);

/// @brief  Look at screen space coordinate. (0,0) top-left, (SCREEN_WIDTH, SCREEN_HEIGHT) bottom-right.
/// @param x_pixels x pixel location.
/// @param y_pixels y pixel location.
void PF_camera_look_at_screen_space(float x_pixels, float y_pixels);

/// @brief Look at a world space coordiate consistent with box2D world.
/// @param x_meter x meter location.
/// @param y_meter y meter location.
void PF_camera_look_at(float x_meter, float y_meter);

/// @brief Updates the camera to preform set functionality called per frame.
/// @see  PF_camera_set_lerp_target
/// @see  PF_camera_set_entity_target
/// @see  PF_camera_set_nothing
void PF_camera_update_camera(void);

/// @brief look at an enetity in the box2D world.
/// @param e pointer to entity.
void PF_camera_set_entity_target(PF_Entity *e );

/// @brief Linearly interpolate to a target point over a duration.
/// @param x_meter target x location.
/// @param y_meter target y location.
/// @param duration time in milliseconds.
void PF_camera_set_lerp_target(float x_meter,float y_meter, Uint64 duration);

/// @brief Clears the current operation of the camera.
void PF_camera_set_nothing(void);

/// @brief Controls an anchor to follow the target entity with some damping.
/// @param target the entity that will be smoothly followed by the camera.
/// @param anchor the entity that the camera actually follows, @see PF_camera_follow_anchor_default.
/// @return the entity that will follow the target.
PF_Entity* PF_camera_follow_init(PF_Entity *target, PF_Entity *anchor );

/// @brief Shift the target by a given displacement in meters.
/// @param dx x displacement.
/// @param dy y displacement.
void PF_camera_follow_target_shift(float dx, float dy);

/// @brief Creates the default camera anchor entity, must be after entity list init.
/// @param positon the starting position of the anchor.
/// @param enable_collisions if the camera is interactable.
/// @param visable if visable, loads `gfx/camera.png` else, loads `gfx/camera_empty.png`.
/// @return the created camera anchor entity.
PF_Entity* PF_camera_follow_anchor_default(b2Vec2 positon, bool enable_collisions, bool visable);

/** @} */

#ifdef __cplusplus
}
#endif
