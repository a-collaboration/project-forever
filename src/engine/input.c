#include "input.h"
#include "../structs.h"
#include "../globals.h"

#include <microui/microui.h>

// Static functions.
/*!
 * The first half of an A-press.
 */
static void do_key_down(SDL_KeyboardEvent *);
static void do_mouse_button_down(SDL_MouseButtonEvent *);
/*!
 * The second half of an A-press.
 */
static void do_key_up(SDL_KeyboardEvent *);
static void do_mouse_button_up(SDL_MouseButtonEvent *);
/*!
 * Move the cursor.
 */
static void do_mouse_move(SDL_MouseMotionEvent *);
/*!
 * Mouse wheel scroll.
 */
static void do_mouse_wheel(SDL_MouseWheelEvent *);
/*!
 * Resize, focus/unfocus, etc.
 */
static void do_window_event(SDL_WindowEvent *);
/*!
 * Fnd the mouse's world location.
 */
void update_mouse_position(void);

// Static constants.
/*!
 * Mappings from SDL keyboard events to mu keys.
 */
static const char mu_key_map[256] = {
  [ SDLK_LSHIFT       & 0xff ] = MU_KEY_SHIFT,
  [ SDLK_RSHIFT       & 0xff ] = MU_KEY_SHIFT,
  [ SDLK_LCTRL        & 0xff ] = MU_KEY_CTRL,
  [ SDLK_RCTRL        & 0xff ] = MU_KEY_CTRL,
  [ SDLK_LALT         & 0xff ] = MU_KEY_ALT,
  [ SDLK_RALT         & 0xff ] = MU_KEY_ALT,
  [ SDLK_RETURN       & 0xff ] = MU_KEY_RETURN,
  [ SDLK_BACKSPACE    & 0xff ] = MU_KEY_BACKSPACE,
};

/*!
 * Mappings from SDL mouse events to mu mouse buttons.
 */
static const char mu_button_map[256] = {
  [ SDL_BUTTON_LEFT   & 0xff ] =  MU_MOUSE_LEFT,
  [ SDL_BUTTON_RIGHT  & 0xff ] =  MU_MOUSE_RIGHT,
  [ SDL_BUTTON_MIDDLE & 0xff ] =  MU_MOUSE_MIDDLE,
};


void handle_input(void)
{
    game.input.mouse_wheel = 0; // always reset mouse wheel.

    SDL_Event event;
    // Handle the full event queue.
    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
        case SDL_QUIT:
            exit(0);
            break;
        case SDL_KEYDOWN:
            do_key_down(&event.key);
            break;
        case SDL_MOUSEBUTTONDOWN:
            do_mouse_button_down(&event.button);
            break;
        case SDL_KEYUP:
            do_key_up(&event.key);
            break;
        case SDL_MOUSEBUTTONUP:
            do_mouse_button_up(&event.button);
            break;
        case SDL_MOUSEMOTION:
            do_mouse_move(&event.motion);
            break;
        case SDL_MOUSEWHEEL:
            do_mouse_wheel(&event.wheel);
            break;
        case SDL_WINDOWEVENT:
            do_window_event(&event.window);
            break;
        case SDL_TEXTINPUT:
            if (game.using_ui)
            {
                mu_input_text(game.p_ctx, event.text.text);
            }
            break;
        default:
            break;
        }
    }
}

static void do_key_down(SDL_KeyboardEvent *event)
{
    // Scancodes
    if (event->repeat == 0 && event->keysym.scancode < 300)
    {
        game.input.keyboard[event->keysym.scancode] = 1;
    }
    if (game.using_ui)
    {
        int c = mu_key_map[event->keysym.sym & 0xff];
        mu_input_keydown(game.p_ctx, c);
    }
}

static void do_mouse_button_down(SDL_MouseButtonEvent *event)
{
    if (event->button < 6)
    {
        game.input.mouse_pressed[event->button] = 1;
    }
    if (game.using_ui)
    {
        int b = mu_button_map[event->button & 0xff];
        mu_input_mousedown(game.p_ctx, event->x, event->y, b);
    }
}

static void do_key_up(SDL_KeyboardEvent *event)
{
    // Scancodes
    if (event->repeat == 0 && event->keysym.scancode < 300)
    {
        game.input.keyboard[event->keysym.scancode] = 0;
    }
    if (game.using_ui)
    {
        int c = mu_key_map[event->keysym.sym & 0xff];
        mu_input_keyup(game.p_ctx, c);
    }
}

static void do_mouse_button_up(SDL_MouseButtonEvent *event)
{
    if (event->button < 6)
    {
        game.input.mouse_pressed[event->button] = 0;
    }
    if (game.using_ui)
    {
        int b = mu_button_map[event->button & 0xff];
        if (game.using_ui)
        {
            mu_input_mouseup(game.p_ctx, event->x, event->y, b);
        }
    }
}

static void do_mouse_move(SDL_MouseMotionEvent *event)
{
    update_mouse_position();
    if (game.using_ui)
    {
        mu_input_mousemove(game.p_ctx, event->x, event->y);
    }
}

static void do_mouse_wheel(SDL_MouseWheelEvent *event)
{
    game.input.mouse_wheel = event->preciseY;
    if (game.using_ui)
    {
        mu_input_scroll(game.p_ctx, 0, event->y * -30);
    }
}

static void do_window_event(SDL_WindowEvent *event)
{
    switch (event->event)
    {
    case SDL_WINDOWEVENT_RESIZED:;
        // Magic ratio by which to shove the viewport upward to get proper scaling.
        float y_adj = (((float)SCREEN_HEIGHT / event->data2) - 1) * event->data2;
        GPU_SetViewport(game.p_screen, GPU_MakeRect(0, y_adj, event->data1, event->data2));
        // GPU_SetVirtualResolution(game.p_screen, SCREEN_WIDTH, SCREEN_HEIGHT);
        GPU_SetVirtualResolution(game.p_screen, event->data1, event->data2);
        game.scaled.x = 0;
        game.scaled.y = y_adj;
        // It may be useful for shader purposes to store the scaled co-ords.
        game.scaled.w = event->data1;
        game.scaled.h = event->data2;

        game.scaled.w_meter = game.scaled.w * METER_PER_PIXEL;
        game.scaled.h_meter = game.scaled.h * METER_PER_PIXEL;
        break;
    default:
        break;
    }
}

void set_pixel_to_world(float pixel_x, float pixel_y, float *world_x, float *world_y)
{
    float screen_delta_meter_x = game.scaled.w_meter * (1.f - 1.f / game.p_screen->camera.zoom_x) / 2.f;
    float screen_delta_meter_y = game.scaled.h_meter * (1.f - 1.f / game.p_screen->camera.zoom_y) / 2.f;

    float screen_size_zoom_meter_x = game.scaled.w_meter / game.p_screen->camera.zoom_x;
    float screen_size_zoom_meter_y = game.scaled.h_meter / game.p_screen->camera.zoom_y;

    float relative_screen_delta_meter_x = screen_size_zoom_meter_x * pixel_x / game.scaled.w;
    float relative_screen_delta_meter_y = screen_size_zoom_meter_y * pixel_y / game.scaled.h;

    *world_x = (float)game.p_screen->camera.x * METER_PER_PIXEL / game.p_screen->camera.zoom_x + screen_delta_meter_x + relative_screen_delta_meter_x;
    *world_y = (float)game.p_screen->camera.y * METER_PER_PIXEL / game.p_screen->camera.zoom_y + screen_delta_meter_y + relative_screen_delta_meter_y;
}

void update_mouse_position(void)
{
    SDL_GetMouseState(&game.input.mouse_x, &game.input.mouse_y);
    set_pixel_to_world(game.input.mouse_x, game.input.mouse_y,  &game.input.mouse_x_meter, &game.input.mouse_y_meter);
}

