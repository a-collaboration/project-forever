#pragma once

#ifdef __cplusplus
extern "C" {
#endif

// Exporting new UI functions is impractical without this include.
#include <microui/microui.h>


/*!
 * Initialise the UI context and inform the state of that.
 * As always, also queues cleanup.
 */
void init_ui(void);

/*!
 * Process the UI elements.
 * Run this after input is handled and before the UI is drawn - doing it with
 * other application logic can make sense - running it before other logic allows
 * it to ensure that you don't double-process input.
 * @param n The number of UI functions to process.
 * @param functions The array of function pointers to process.
 */
void process_ui(int, void (**)(void));

/*!
 * Go through the steps to draw the UI.
 * If it is initialised, do this at the end of your draw process.
 * This doesn't do anything for safety, so be careful.
 */
void draw_ui(void);

/*!
 * @defgroup uielements
 * Custom UI elements for `microui`. Defined in a way consistent with the library.
 * @{
 */

/// An extension to the MU_COMMAND enum.
#define MU_COMMAND_IMAGE -1

/// For each new type, manually add it to the union `mu_Command` in microui.h.

/*!
 * Support for drawing an image as part of a UI element.
 * Assumes a texture atlas with image properties on offer, but mostly just
 * tries to determine the sizes and hands over a src_rec and dst_rec consistent
 * with icons (which work the same way).
 * @param ctx The mu_Context in which the draw is evaluated.
 * @param key The key to the image metadata in the atlas.
 * @param dst The destination rectangle.
 */
void mu_draw_image(mu_Context *, const char *, mu_Rect);

/*!
 * A toggleable button which displays an icon rather than text.
 * Takes a function argument to determine whether it is selected, based on its index.
 * Designed for radio buttons, but can be more broadly applicable.
 * @param ctx The mu_Context in which to create the element.
 * @param filename The filename of the image to display on the button.
 * @param icon The icon to display when the button is selected.
 * @param opt Additional flags to pass to microui.
 */
#define mu_picture_toggle(ctx, filename, state)    mu_picture_toggle_ex(ctx, filename, (state != 0) * MU_ICON_CLOSE, MU_OPT_ALIGNCENTER)
int mu_picture_toggle_ex(mu_Context *, const char *, int, int);

/*! @} */

#ifdef __cplusplus
}
#endif
