#pragma once

#include <box2d/geometry.h>
#include <box2d/manifold.h>
#ifdef __cplusplus
extern "C" {
#endif

#include "../defs.h"

// Tile facing can be crammed into the biggest bits of an int.
#define TILE_FACING_MASK 0b11000000000000000000000000000000

/*!
 * Details for creating and handling terrain tiles.
 * Define states here to allow for simplification.
 */

/*!
 * Tile Structures
 * Defines the hitbox of a tile. For simplicity, we allow for heights compatible
 * with full, half and quarter steps.
 * Quarter slopes are omitted for now but included in the enum.
 * Annotated until we come up with a more readable form.
 */
enum tile_structure {
    TILE_STR_EMPTY       = 0, // Nothing there.
    TILE_STR_FULL        = 1,
    TILE_STR_LOW         = 2,
    TILE_STR_MED         = 3,
    TILE_STR_HIGH        = 4,
    TILE_STR_SF_A        = 5,
    TILE_STR_SF_D        = 6,
    TILE_STR_SH_LOW_A    = 7,
    TILE_STR_SH_LOW_D    = 8,
    TILE_STR_SH_HIGH_A   = 9,
    TILE_STR_SH_HIGH_D   = 10,
    TILE_STR_SQ_LOW_A    = 11,
    TILE_STR_SQ_LOW_D    = 12,
    TILE_STR_SQ_LMED_A   = 13,
    TILE_STR_SQ_LMED_D   = 14,
    TILE_STR_SQ_HMED_A   = 15,
    TILE_STR_SQ_HMED_D   = 16,
    TILE_STR_SQ_HIGH_A   = 17,
    TILE_STR_SQ_HIGH_D   = 18,
};

/*!
 * Tile Facings
 * Determins the orientation of a tile. This lets us rotate a tile as required.
 */
enum tile_facing {
    TILE_FACING_UP,
    TILE_FACING_LEFT,
    TILE_FACING_DOWN,
    TILE_FACING_RIGHT
};

/*!
 * A struct containing the details for each tile. These are held in a static
 * array for easy reference.
 */
typedef struct PF_AtlasImage PF_AtlasImage;
struct PF_Tile {
    char label[MAX_FILENAME_LENGTH];
    PF_AtlasImage *texture; // Maybe include the full image to avoid the double redirection.
    enum tile_structure hitbox;
};

/*!
 * Intiialise a global tile data structure.
 */
void init_tiles(void);

/*!
 * Get the hitbox for a tile, based on the enum.
 * Returns an appropriate Box2D polygon.
 * @param tile_data Bitfield (somewhat) of tile information.
 */
typedef struct b2Polygon b2Polygon;
b2Polygon get_tile_hitbox(uint32_t);

#ifdef __cplusplus
}
#endif
