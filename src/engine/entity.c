#include "entity.h"
#include <stb/stb_ds.h>

#include "../globals.h"

#include <box2d/box2d.h>
#include <box2d/geometry.h>

const uint32_t CATEGORY_DEFAULT = 0x00000001;
const uint32_t CATEGORY_TERRAIN = 0x00000002;
const uint32_t CATEGORY_FOREGROUND = 0x00000004;
const uint32_t CATEGORY_BACKGROUND = 0x00000008;

PF_Entity *spawn_box_from_image(const char *texture_path,b2BodyDef body_def,b2ShapeDef shape_def)
{
    // Get texture and it's dimensions
    PF_AtlasImage *texture = &shget(game.p_sprite_atlas, texture_path);
    float width = (texture->rect.w) * METER_PER_PIXEL / 2.f;
    float height = (texture->rect.h) * METER_PER_PIXEL / 2.f;

    // Create body
    b2BodyId box_body_id;
    box_body_id = b2CreateBody(world.world_id, &body_def);
    // Create box shape
    b2Polygon test_box = b2MakeBox(width, height);
    // Attach shape to body
    b2ShapeId box_shape_id;
    box_shape_id = b2CreatePolygonShape(box_body_id, &shape_def, &test_box);

    // Define the entity
    PF_Entity *e = calloc(1, sizeof(PF_Entity));
    e->body_id = box_body_id;
    e->shape_id = box_shape_id;
    e->texture = texture;
    // Store in World's linked list
    e->next = NULL;
    world.p_entity_tail->next = e;
    world.p_entity_tail = e;

    return e;
}

PF_Entity *spawn_circle_from_image(const char *texture_path,b2BodyDef body_def,b2ShapeDef shape_def)
{
    // Get texture and it's dimensions
    PF_AtlasImage *texture = &shget(game.p_sprite_atlas, texture_path);
    float width = (texture->rect.w) * METER_PER_PIXEL / 2.f;
    float height = (texture->rect.h) * METER_PER_PIXEL / 2.f;

    // Create body
    b2BodyId box_body_id;
    box_body_id = b2CreateBody(world.world_id, &body_def);
    // Create box shape
    b2Circle test_circle ;
    test_circle.radius = width;
    test_circle.point.x = 0;
    test_circle.point.y = 0;
    // Attach shape to body
    b2ShapeId box_shape_id;
    box_shape_id = b2CreateCircleShape(box_body_id, &shape_def, &test_circle);

    // Define the entity
    PF_Entity *e = calloc(1, sizeof(PF_Entity));
    e->body_id = box_body_id;
    e->shape_id = box_shape_id;
    e->texture = texture;
    // Store in World's linked list
    e->next = NULL;
    world.p_entity_tail->next = e;
    world.p_entity_tail = e;

    return e;
}