#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <SDL2/SDL_gpu.h>

/*!
 * @defgroup Font
 * Interface for displaying TTF text.
 * These functions are written for SDL_gpu, with heavy overlap in the interface
 * in `draw.h`. This utilises STB's text support, both to be more renderer agnostic
 * in the long run and also because SDL_ttf is extremely heavy.
 * @{
 */
typedef struct PF_Font PF_Font;

/*!
 * Open a TTF font with the given name, to a specified GPU target.
 * Afterwards, the font does need to be freed.
 * This creates a texture atlas with prerendered ASCII characters.
 * TODO: allow extending to UTF8 or similar.
 * @param target The GPU target to which to render.
 * @param filename The path to the font to render.
 * @param size The font size.
 */
PF_Font* PF_font_open(GPU_Target*, const char*, float);

/*!
 * Close a TTF font and free all the memory and textures associated.
 * @param font The font to close.
 */
void PF_font_close(PF_Font*);

/*!
 * Draw some text in the specified colour.
 * Characters are copied from the texture atlas.
 * Currently, only ASCII characters are supported; unsupported chars are ignored.
 * @param target The GPU target to use.
 * @param font The font with which to draw.
 * @param x The x co-ordinate to use.
 * @param y The y co-ordinate to use.
 * @param colour The colour in which to print the text.
 * @param text The text to print.
 */
void PF_font_blit_text(GPU_Target*, PF_Font*, float, float, SDL_Color, const char*);

/*!
 * Return the length in pixels of a text.
 * The height is accessible through font->baseline.
 * @param font The font to use.
 * @param text The string to measure.
 */
float PF_font_measure_text(PF_Font*, const char*);

/*!
 * Unfortunately, we're almost doing OOP here to keep the header size small.
 * As such, a getter for font height.
 * @param font The font to use.
 */
float PF_font_text_height(PF_Font*);

/*! @} */

#ifdef __cplusplus
}
#endif
