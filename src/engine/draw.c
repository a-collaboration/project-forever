#include "draw.h"
#include "init.h"
#include "../structs.h"
#include <SDL2/SDL_gpu.h>
#include <iso646.h>
#include <jansson.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>

// STB - let it compile here.
#define STB_DS_IMPLEMENTATION
#include <stb/stb_ds.h>

#include "../globals.h"

/*!
 * Initialise a single shader pipeline.
 * Tries to preserve sanity by using vector operations from stb_ds.
 * @param index The index at which the shader is to be stored and referenced.
 * @param vert Path to a vertex shader.
 * @param frag Path to a fragment shader.
 * @param n_res Number of uniforms to store.
 * @param ... The name of each uniform. These locations will be store in the provided order.
 */
static void init_shader(uint32_t, const char*, const char*, uint32_t, ...);

void init_graphics(const char *title)
{
    /* SDL_gpu Initialisation */
    int sdl_flags = SDL_WINDOW_RESIZABLE;

    game.p_screen = GPU_Init(SCREEN_WIDTH, SCREEN_HEIGHT, sdl_flags); // Get the size from defs.
    if (game.p_screen == NULL) {
        printf("Unable to initialise SDL_gpu renderer.\n");
        exit(1);
    }
    // Get the underlying window and set the title.
    uint32_t window_id = game.p_screen->renderer->current_context_target->context->windowID;
    game.p_window = SDL_GetWindowFromID(window_id);
    SDL_SetWindowTitle(game.p_window, title);

    game.scaled.w = SCREEN_WIDTH;
    game.scaled.h = SCREEN_HEIGHT;
    game.scaled.w_meter = SCREEN_WIDTH_METER;
    game.scaled.h_meter = SCREEN_HEIGHT_METER;
    game.scaled.x = 0;
    game.scaled.y = 0;

    push_deletion(GPU_Quit); // Close SDL and SDL_gpu once we're done.
}

void init_atlas(void)
{
    // Ensure that the atlas is NULL:
    game.p_sprite_atlas = NULL;
    // Load the atlas.
    GPU_Image *atlas_texture = load_texture("dat/atlas.png");
    // Load the metadata.
    json_t *atlas_meta;
    json_error_t error;
    atlas_meta = json_load_file("dat/atlas.json", 0, &error);
    if (!atlas_meta || !json_is_array(atlas_meta))
    {
        SDL_Log("Something happened: %s at %s.\n", error.text, error.source);
    }
    // Construct the hash table from the data.
    size_t index;
    json_t *node = NULL;
    // Fill array of `atlasImage`s with data from the metadata. Free it, and the hashmap, at cleanup.
    game.p_sprites = calloc(json_array_size(atlas_meta), sizeof(PF_AtlasImage));
    // Jansson supports a foreach, making a macro that loops over the array.
    json_array_foreach(atlas_meta, index, node)
    {
        if (!json_is_object(node))
        {
            SDL_Log("Something went wrong with node %zu of atlas.json.\n", index);
            continue; // Complain about malformed JSON, but try to continue.
        }
        strncpy(game.p_sprites[index].filename,
                json_string_value(json_object_get(node, "tag")),
                MAX_FILENAME_LENGTH);
        game.p_sprites[index].rect.x = json_integer_value(json_object_get(node, "x"));
        game.p_sprites[index].rect.y = json_integer_value(json_object_get(node, "y"));
        game.p_sprites[index].rect.w = json_integer_value(json_object_get(node, "w"));
        game.p_sprites[index].rect.h = json_integer_value(json_object_get(node, "h"));
        game.p_sprites[index].texture = atlas_texture;
        shput(game.p_sprite_atlas, game.p_sprites[index].filename, game.p_sprites[index]);
    }
    // Save the atlas texture to the game so that it won't be leaked.
    game.p_atlas_texture = atlas_texture;
    json_decref(atlas_meta);
    push_deletion(cleanup_atlas); // Free the atlas once we're done.
}

void init_shaders(void)
{
    // Be reasonable and maintain an enum of pipelines in defs for ease of reference.
    game.n_shaders = SHADER_MAX;
    game.p_shaders = calloc(SHADER_MAX, sizeof(PF_ShaderData));
    init_shader(SHADER_COLOURS, "shd/common.vert", "shd/colour.frag", 1, "u_colour");
    init_shader(SHADER_OPACITY, "shd/common.vert", "shd/opacity.frag", 1, "u_alpha");
    push_deletion(cleanup_shaders);
}

static void init_shader(uint32_t index, const char* vert, const char* frag, uint32_t n_res, ...)
{
    PF_ShaderData data = {};
    data.block = load_shader_block(&data.shader, vert, frag);
    data.n_res = n_res;
    data.p_res = calloc(n_res, sizeof(int));
    va_list ap;
    va_start(ap, n_res);
    for (int i = 0; i < n_res; ++i) {
        data.p_res[i] = GPU_GetUniformLocation(data.shader, va_arg(ap, const char *));
    }
    va_end(ap);
    game.p_shaders[index] = data;
}


void cleanup_atlas(void)
{
    free(game.p_sprites);
    hmfree(game.p_sprite_atlas);
    GPU_FreeImage(game.p_atlas_texture);
}

void cleanup_shaders(void)
{
    for (int i = 0; i < game.n_shaders; ++i) {
        GPU_FreeShaderProgram(game.p_shaders[i].shader);
        if (game.p_shaders[i].n_res > 0)
            free(game.p_shaders[i].p_res);
    }
    free(game.p_shaders);
}

void prepare_scene(void)
{
    GPU_ClearRGB(game.p_screen, 25, 25, 25);
}

void present_scene(void)
{
    GPU_Flip(game.p_screen);
}

void blit_image(PF_AtlasImage *img, float x, float y, float angle, float scale)
{
    // Make drawing consistent with using radians everywhere else.
    angle = angle * 180.f/PI;    
    GPU_BlitTransform(img->texture, &img->rect, game.p_screen, x, y, angle, scale, scale);
}

GPU_Image* load_texture(const char *filename)
{
    GPU_Image* texture = NULL;
    // Add logging here if necessary.
    texture = GPU_LoadImage(filename);
    return texture;
}

GPU_ShaderBlock load_shader_block(uint32_t *p, const char *vert_path, const char *frag_path)
{
    uint32_t v, f;
    v = GPU_LoadShader(GPU_VERTEX_SHADER, vert_path);
    if (!v)
        GPU_LogError("Failed to load vertex shader (%s): %s", vert_path, GPU_GetShaderMessage());
    f = GPU_LoadShader(GPU_FRAGMENT_SHADER, frag_path);
    if (!f)
        GPU_LogError("Failed to load fragment shader (%s): %s", frag_path, GPU_GetShaderMessage());
    *p = GPU_LinkShaders(v, f);
    if (p < 0) printf("%s\n", GPU_GetShaderMessage());
    if (!p) {
        GPU_ShaderBlock b = {-1, -1, -1, -1};
        GPU_LogError("Failed to link shader programme (%s + %s): %s", vert_path, frag_path, GPU_GetShaderMessage());
        return b;
    }
    // Pass through position, texcoord, colour and model view projection matrix.
    GPU_ShaderBlock block = GPU_LoadShaderBlock(*p, \
        "gpu_Vertex", "gpu_TexCoord", "gpu_Colour", "gpu_ModelViewProjectionMatrix");

    return block;
}

void activate_shader(enum shaders shader)
{
    GPU_ActivateShaderProgram(
            game.p_shaders[shader].shader,
            &game.p_shaders[shader].block
    );
}
