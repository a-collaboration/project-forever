#include <SDL2/SDL.h>
#include <string.h> // For processing input.

#include "defs.h"
#include "structs.h"
#include "engine/init.h"
#include "engine/draw.h"
#include "engine/input.h"
#include "game/level.h"
#include "editor/mapedit.h"

// Global struct with game details. We might want to make this non-global.
PF_Game game = {};
PF_World world = {};
int run;

int main(int argc, char *argv[])
{
    char mode = START_MODE_GAME;
    // If applicable, parse the startup arguments.
    // For the time being, just search for `-e` or `-v`.
    if (argc > 1) {
        if (argv[1][0] == '-' && strlen(argv[1]) > 0) {
            switch (argv[1][1]) {
                case 'e':
                    mode = START_MODE_EDITOR;
                    break;
                case 'v':
                    printf("Version 0.0.3: Proper Semver TBA\n");
                    exit(0);
                    break;
                default: 
                    break;
            }
        }
    }

    // Initialise.
    init("Project Forever");
    // Set the cleanup function.
    atexit(cleanup);

    run = 1;

    switch (mode){
        default:
        case START_MODE_GAME:
            init_game_world();
            mode_game_activate();
            break;
        case START_MODE_EDITOR:
            init_editor((argc > 2) ? argv[2] : NULL);
            mode_editor_activate();
            break;
    }


    while (run)
    {
        handle_input();
        prepare_scene();
        game.delegate.logic();
        game.delegate.draw();
        present_scene();
    }

    return 0;
}
