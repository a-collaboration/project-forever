/*!
 * Forward declarations and API for sprite atlas generation.
 * Note that `atlas.c` also contains a main function that can be used but won't be exported.
 */
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * The function that actually generates an atlas.
 * @param src - The source directory.
 * @param dst - The destination directory.
 */
void compile_sprite_atlas(const char *, const char *);

/*!
 * A node in a tree, used by the atlas.
 */
typedef struct AtlasNode
{
    int x, y;
    int w, h;
    int used;

    struct AtlasNode *side_child, *down_child;
} AtlasNode;

/*!
 * An SDL image and its tagging metadata - here, its filename.
 **/
typedef struct SDL_Surface SDL_Surface;
typedef struct AtlasItem
{
    SDL_Surface *surface;
    char *tag;
} AtlasItem;

#ifdef __cplusplus
}
#endif
