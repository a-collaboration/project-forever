/*!
 * Simple application to generate a sprite atlas using SDL.
 * Takes two arguments - --src for the source dir and --dst for dest file.
 * Also accepts -i and -o, or if there are just two parameters, assumes that order.
 */
#include "atlas.h"

#include <SDL2/SDL.h>
#include <jansson.h>
#include <string.h>

#ifdef _WIN32
#include "../../libs/dirent/dirent.h" // Supposedly Windows-portable.
#else
#include <dirent.h>
#endif


// #include "../../libs/SDL_gpu/include/SDL_gpu.h"
#include <SDL2/SDL_gpu.h>
#include "../defs.h"

// Declare an image array for this file.
GPU_Target *target;
static AtlasItem *images;

/*! 
 * Some static functions to assist in atlas generation.
 **/
static int image_comparator(const void(*), const void(*));
static int count_files(const char*);
static void load_sprites(int*, const char*);
static int init_sprites(const char*);
static void split_node(AtlasNode *, int, int);
static AtlasNode* find_node(AtlasNode*, int, int);
static void free_atlas(AtlasNode*);

int main (int argc, char *argv[])
{
    char *src = NULL, *dst = NULL;
    target = GPU_Init(SCREEN_HEIGHT, SCREEN_WIDTH, SDL_INIT_VIDEO);
    // Parse the command args.
    switch (argc)
    {
        case 3:
            src = argv[1];
            dst = argv[2];
            break;
        case 5:
            if (argv[1][0] == '-') {
                if (argv[1][1] == 'i' || (argv[1][1] == '-' && argv[1][2] == 's')) {
                    src = argv[2];
                    dst = argv[4];
                } else {
                    src = argv[4];
                    dst = argv[2];
                }
            }
            break;
        case 1:
        default:
            src = "gfx";
            dst = "dat";
            break;
    }
    compile_sprite_atlas(src, dst);
    GPU_Quit();
    return 0;
}

void compile_sprite_atlas(const char *src, const char *dst)
{
    SDL_Log("Compiling Sprite Atlas.\n");
    AtlasNode *root, *n;
    SDL_Surface *atlas;
    SDL_Rect dest;
    json_t *json_obj, *json_root = json_array();
    int n_images, rotated, w, h, rotations = 0;
    // Make the preparations.
    n_images = init_sprites(src);
    if (n_images == 0)
        return;
    SDL_Log("Found %d images.\n", n_images);
    root = malloc(sizeof(AtlasNode));
    root->used = 0;
    root->x = 0;
    root->y = 0;
    root->w = 1024; // Replace magic numbers by some calculated constant.
    root->h = 1024; // As above.
    atlas = SDL_CreateRGBSurface(0, 1024, 1024, 32, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);
    // Create the atlas, freeing the images as required.
    for (int i = 0; i < n_images; ++i)
    {
        rotated = 0;
        w = images[i].surface->w;
        h = images[i].surface->h;
        n = find_node(root, w, h);
        if (n == NULL)
        {
            rotated = 1;
            n = find_node(root, h, w);
        }
        if (n != NULL)
        {
            if (rotated)
            {
                n->h = w;
                n->w = h;
                ++rotations;
            }
            dest.x = n->x;
            dest.y = n->y;
            dest.w = n->w;
            dest.h = n->h;
            if (!rotated)
            {
                SDL_BlitSurface(images[i].surface, NULL, atlas, &dest);
            } else {
                // SDL rotation is a pain. For now, do nothing.
                // Come back and fix this if sprites ever fail to appear.
            }
            // Append to the JSON metadata.
            json_obj = json_object();
            json_object_set_new(json_obj, "tag", json_string(images[i].tag));
            json_object_set_new(json_obj, "x", json_integer(dest.x));
            json_object_set_new(json_obj, "y", json_integer(dest.y));
            json_object_set_new(json_obj, "w", json_integer(dest.w));
            json_object_set_new(json_obj, "h", json_integer(dest.h));
            json_object_set_new(json_obj, "rotated", json_integer(rotated));
            json_array_append(json_root, json_obj);
            json_decref(json_obj);
            printf("[%04d / %04d] %s\n", i+1, n_images, images[i].tag);
        }
        SDL_FreeSurface(images[i].surface);
        free(images[i].tag);
    }
    char out_img[MAX_FILENAME_LENGTH] = {}, out_meta[MAX_FILENAME_LENGTH] = {};
    snprintf(out_img, MAX_FILENAME_LENGTH, "%s/atlas.png", dst);
    snprintf(out_meta, MAX_FILENAME_LENGTH, "%s/atlas.json", dst);
    GPU_SaveSurface(atlas, out_img, GPU_FILE_PNG);
    SDL_FreeSurface(atlas);
    // Write the metadata.
    FILE *fp = fopen(out_meta, "wb");
    json_dumpf(json_root, fp, JSON_INDENT(2));
    fclose(fp);
    json_decref(json_root);
    SDL_Log("Sprite atlas compiled. Proceeding.\n");
    free_atlas(root); // Recursively frees the root and all of its children.
    free(images);

}


/*!
 * Implementations of static functions.
 */
static int image_comparator(const void *a, const void *b)
{
    // Ternary operator for image comparison, by height.
    AtlasItem *i1 = (AtlasItem*)a;
    AtlasItem *i2 = (AtlasItem*)b;
    return i2->surface->h - i1->surface->h;
}

static int count_files(const char *dir)
{
    /* Recursively traverses a directory, to locate the image files within. */
    DIR *d;
    struct dirent *ent;
    char *path;
    int i = 0;

    if ((d = opendir(dir)) != NULL)
    {
        while ((ent = readdir(d)) != NULL)
        {
            if (ent->d_type == DT_DIR)
            {
                if (ent->d_name[0] != '.')
                {
                    path = malloc(strlen(dir) + strlen(ent->d_name) + 2);
                    sprintf(path, "%s/%s", dir, ent->d_name);
                    i += count_files(path);
                    free(path);
                }
            }
            else
            {
                ++i;
            }
        }
        closedir(d);
    }
    return i;
}

static void load_sprites(int *i, const char *dir)
{
    // Do the actual loading of the sprite data into the images array.
    DIR *d;
    struct dirent *ent;
    char *path;

    if ((d = opendir(dir)) != NULL)
    {
        while ((ent = readdir(d)) != NULL)
        {
            path = malloc(strlen(dir) + strlen(ent->d_name) + 2);
            if (ent->d_type == DT_DIR)
            {
                if (ent->d_name[0] != '.')
                {
                    sprintf(path, "%s/%s", dir, ent->d_name);
                    load_sprites(i, path);
                }
            }
            else
            {
                sprintf(path, "%s/%s", dir, ent->d_name);
                images[*i].surface = GPU_LoadSurface(path);
                if (images[*i].surface)
                {
                    images[*i].tag = malloc(strlen(path) + 1);
                    strcpy(images[*i].tag, path);
                    SDL_SetSurfaceBlendMode(images[*i].surface, SDL_BLENDMODE_NONE);
                    *i = *i + 1;
                }
            }
            free(path);
        }
        closedir(d);
    }
}

static int init_sprites(const char *src)
{
    // Create a global array of sprites, load in the data, then return its size.
    int n_images, i = 0;
    n_images = count_files(src);
    if (n_images > 0){
        images = calloc(n_images, sizeof(AtlasItem));
        load_sprites(&i, src);
        qsort(images, i, sizeof(AtlasItem), image_comparator);
    }
    return n_images;
}

static void split_node(AtlasNode *node, int w, int h)
{
    int padding = 1;
    node->used = 1;

    node->side_child = calloc(1, sizeof(AtlasNode));
    node->down_child = calloc(1, sizeof(AtlasNode));

    node->side_child->x = node->x + w + padding;
    node->side_child->y = node->y;
    node->side_child->w = node->w - w - padding;
    node->side_child->h = h;

    node->down_child->x = node->x;
    node->down_child->y = node->y + h + padding;
    node->down_child->w = node->w;
    node->down_child->h = node->h - h - padding;

    return;
}

static AtlasNode *find_node(AtlasNode *root, int w, int h)
{
    if (root->used)
    {
        AtlasNode *n = NULL;
        if ((n = find_node(root->side_child, w, h)) != NULL || (n = find_node(root->down_child, w, h)) != NULL)
            return n;
    } else if ( w <= root->w && h <= root->h) {
        split_node(root, w, h);
        return root;
    }
    return NULL;
}

static void free_atlas(AtlasNode *node)
{
    // Could cause a stack overflow if the tree is too large.
    if (node == NULL)
        return;
    // Recursively free the children
    free_atlas(node->side_child);
    free_atlas(node->down_child);
    free(node);
    return;
}
