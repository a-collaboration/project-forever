# Project Forever (Final Name Pending)

A game utilising SDL_gpu for graphics and the Box2D Physics Engine.

## Build Instructions

For graphics, `cd` in `libs/sdl-gpu`, configure `CMakeLists.txt` to your requirements and then run `cmake CMakeLists.txt` followed by building. Move the compiled `SDL_gpu` directory into `libs`.

Finally, navigate to the main directory, run `cmake CMakeLists.txt` and build. The final build will be in `bin/forever`.
